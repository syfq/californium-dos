set nokey
set key left top
set xlabel "Time"
set ylabel "Client"
set yrange [0:4]
set ytics (0, "Client 1" 1, "Client 2" 2, "Client 3" 3)
#set xrange [0:]
set xdata time
#set timefmt "%Y-%m-%d %H:%M:%S"
set timefmt "%H:%M:%S"
#plot 'smack/client1/time_elapsed/c1_rtt_6.log' using ($0+1):6 with lines title "adaptive smack", 'plain/client1/time_elapsed/c1_rtt_6.log' using ($0+1):7 with lines title "plain"
#plot 'smack/client3/time_elapsed/c3_rtt_60.log' using ($0+1):6 with lines title "adaptive smack", 'plain/client1/time_elapsed/c1_rtt_60.log' using ($0+1):7 with lines title "plain"
#plot 'smack/client1/time_elapsed/c1_rtt_120.log' using ($0+1):6 with lines title "adaptive smack", 'plain/client1/time_elapsed/c1_rtt_120.log' using ($0+1):7 with lines title "plain"
#plot 'smack/client1/time_elapsed/c1_rtt_180.log' using ($0+1):6 with lines title "adaptive smack", 'plain/client1/time_elapsed/c1_rtt_180.log' using ($0+1):7 with lines title "plain"

plot 'pattern/c1.log' every ::2::21 using 2:6 title "client 1 receive response", 'pattern/c2.log' every ::2::21 using 2:6 title "client 2 receive response", 'pattern/c3.log' every ::1::20 using 2:6 title "client 3 receive response"

#plot 'smack/client1/time_elapsed/c1_rtt_60.log' every ::2::21 using 2:6 title "client 1 receive response", 'smack/client2/time_elapsed/c2_rtt_60.log' every ::1::20 using 2:6 title "client 2 receive response", 'smack/client3/time_elapsed/c3_rtt_60.log' every ::1::20 using 2:6 title "client 3 receive response"

#plot 'smack/client1/time_elapsed/c1_rtt_120.log' every ::3::22 using 2:6 title "client 1 receive response", 'smack/client2/time_elapsed/c2_rtt_120.log' every ::1::20 using 2:6 title "client 2 receive response", 'smack/client3/time_elapsed/c3_rtt_120.log' every ::1::20 using 2:6 title "client 3 receive response"

#plot 'smack/client1/log/c1_rtt_180.log' every ::1::20 using 2:6 title "client 1 receive response", 'smack/client2/log/c2_rtt_180.log' every ::1::20 using 2:6 title "client 2 receive response", 'smack/client3/log/c3_rtt_180.log' every ::1::20 using 2:6 title "client 3 receive response"
