set nokey
set key left top
set xlabel "Request #"
set ylabel "Rount Trip Time (ms)"
#set yrange [0:40000]
set yrange [0:90]
plot 	'adaptive/client1/120.log' lt rgb "#2E86C1" with lines title 'Adaptive Californium', \
		'adaptive/client1/120.log' lt rgb "#AED6F1" with errorbars notitle, \
		'vanilla/client1/120.log' lt rgb "#F39C12" with lines title 'Vanilla Californium', \
		'vanilla/client1/120.log' lt rgb "#F9E79F" with errorbars notitle

