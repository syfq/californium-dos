import sys

filename = 'main0.log'
file = open(filename,'r')
n = 0
for line in file:
    n = n+1
    val = line.split()
    total = 0
    for a in val:
        total = total + float(a)

    # average
    avg = float(total)/len(val)
    # print avg

    # standard deviation
    atas = 0
    for a in val:
        atas = atas + (float(a)-avg)**2
    sd = (atas/len(val))**0.5
    # print sd

    # margin error
    me = 1.96 * sd / float(len(val))**0.5
    print n, avg, me
