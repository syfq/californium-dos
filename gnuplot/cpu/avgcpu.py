import time, os

#Set the filename and open the file
filename = 'cpumem_0.log'
file = open(filename,'r')

#Find the size of the file and move to the end
#st_results = os.stat(filename)
#st_size = st_results[6]
#file.seek(st_size)

prevuser	= 0
prevnice	= 0
prevsystem	= 0
previdle	= 0
previowait	= 0
previrq		= 0
prevsoftirq = 0
prevsteal	= 0
average = 0
tots = 0
n = 0

while 1:
    where = file.tell()
    line = file.readline()
    #print line
    if not line:
		print tots/n
		break
    #    time.sleep(1)
    #    file.seek(where)
    else:
		val = line.split(" ")
		user = int(val[3])
		nice = int(val[4])
		system = int(val[5])
		idle = int(val[6])
		iowait = int(val[7])
		irq = int(val[8])
		softirq = int(val[9])
		steal = int(val[10])

		PrevIdle = previdle + previowait
		Idle = idle + iowait

		PrevNonIdle = prevuser + prevnice + prevsystem + previrq + prevsoftirq + prevsteal
		NonIdle = user + nice + system + irq + softirq + steal

		PrevTotal = PrevIdle + PrevNonIdle
		Total = Idle + NonIdle

		#if PrevTotal != 0:
		totald = Total - PrevTotal
		idled = Idle - PrevIdle
		CPU_Percentage = (float(totald) - float(idled))/float(totald)
		prevuser    = user
		prevnice    = nice
		prevsystem  = system
		previdle    = idle
		previowait  = iowait
		previrq     = irq
		prevsoftirq = softirq
		prevsteal   = steal
		
		tots = tots + (CPU_Percentage*100)
		n = n+1
		#print prevuser, prevnice, prevsystem, previdle, previowait, previrq, prevsoftirq, prevsteal
		#print user, nice, system, idle, iowait, irq, softirq, steal
		#Total_CPU_time_since_boot = user+nice+system+idle+iowait+irq+softirq+steal
		#Total_CPU_Idle_time_since_boot = idle + iowait
		#Total_CPU_usage_time_since_boot = Total_CPU_time_since_boot - Total_CPU_Idle_time_since_boot
		#Total_CPU_percentage = (Total_CPU_usage_time_since_boot/Total_CPU_time_since_boot)*100

