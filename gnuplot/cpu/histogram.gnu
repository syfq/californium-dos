#set nokey
#set key left top
#clear
#reset
#unset key
#set key autotitle columnheader
#set style data histogram
#set style fill solid border
#set xlabel "Attack per sampling time"
#set ylabel "Time spent each phase (sec)"
#set style histogram columnstacked
#set boxwidth 0.6 relative
#plot for [COL=2:5] 'histo.log' using COL title "a"
#plot 'histo.log' using 1 title "test"

# set terminal pngcairo  transparent enhanced font "arial,10" fontscale 1.0 size 600, 400 
# set output 'histograms.4.png'
set boxwidth 0.75 absolute
set style fill   solid 1.00 border lt -1
set key inside left top vertical Left reverse noenhanced autotitle columnhead nobox
set key invert samplen 4 spacing 1 width 0 height 0 
#set style histogram rowstacked title textcolor lt -1
set style histogram title textcolor lt -1
#set minussign
set datafile missing '-'
set style data histograms
set xtics border in scale 0,0 nomirror rotate by -45  autojustify
set xtics  norangelimit 
set xtics   ()
set xlabel "Attack Configuration"
set ylabel "Average RAM Utilization (MB)"
set yrange [0:500]
#set yrange [ 0.00000 : 7.00000e+06 ] noreverse nowriteback
#x = 0.0
#i = 4
## Last datafile plotted: "immigration.dat"
plot 'mem.txt' using 2:xtic(1), for [i=3:4] '' using i
