set nokey
set key left top
set xlabel "Request #"
set ylabel "Rount Trip Time (ms)"
set xrange [0:50] 
set xtics 0,5,50
set yrange [0:40000]
plot 'adaptive/client1/180/4.log' u ($0+1):1 lt rgb "#2E86C1" with lines title 'Adaptive Californium', \
	 'vanilla/client1/180/3.log' u ($0+1):1 lt rgb "#F39C12" with lines title 'Vanilla Californium',
#'Smack/client/rtt/120.log' with errorbars title '95% Confidence CoAP Adaptive SMACK', \
#'Plain/client/rtt/120.log' with errorbars title '95% confidence Basic CoAP'

#set yrange [0:40000]
#set xrange [0:]

#set table 'lower.dat'
#plot 'Smack/client/rtt/60.log' using 1:($2-$3) 
#set table 'upper.dat'
#plot 'Smack/client/rtt/60.log' using 1:($2+$3)
#set table 'lower_pl.dat'
#plot 'Plain/client/rtt/60.log' using 1:($2-$3)
#set table 'upper_pl.dat'
#plot 'Plain/client/rtt/60.log' using 1:($2+$3)
#unset table
#set terminal pngcairo
#set output 'data.png'
#set style fill transparent solid 0.2 noborder
#plot '< paste lower.dat upper.dat' using 1:2:5 with filledcurves title '95% Confidence CoAP Adaptive SMACK', \
#     'Smack/client/rtt/60.log' using 1:2 with lines lt 1 smooth cspline title 'Mean Value CoAP Adaptive SMACK',\
#	 '< paste lower_pl.dat upper_pl.dat' using 1:2:5 with filledcurves title '95% confidence Basic CoAP', \
#     'Plain/client/rtt/60.log' using 1:2 with lines lt 2 smooth cspline title 'Mean Value Basic CoAP'

