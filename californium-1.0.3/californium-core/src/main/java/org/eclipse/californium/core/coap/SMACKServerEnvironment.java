package org.eclipse.californium.SMACKEnvironment;

import java.util.Map;
import java.util.HashMap;
//import org.eclipse.californium.coap.EndpointAddress; //Disabled //Rikard
//import org.eclipse.californium.util.Properties; //Disabled //Rikard
import org.eclipse.californium.coap.SMACKengine.SMACKEngine;

public class SMACKServerEnvironment extends SMACKEnvironment {

  private Map<Integer, SMACKServerSession> sessions;
  private byte[] KM = new byte[keySize];
  private byte[] seed = new byte[keySize];
  private byte[] KMS = new byte[keySize];

  public SMACKServerEnvironment(byte[] masterKey, byte[] s) {

     sessions = new HashMap<Integer, SMACKServerSession>();
     System.arraycopy(masterKey, 0, KM, 0, masterKey.length);
     System.arraycopy(s, 0, seed, 0, s.length);
     KMS = SMACKEngine.computeKey(KM, seed);

    // debug
    /*
    System.out.println("KMS");
    for (byte bi : KMS)
	System.out.format("0x%x ", bi);
    System.out.println("\n");
    */

  }

  public byte[] getMasterKey() { return KM; }

  public byte[] getSeed() { return seed; }

  public byte[] getMasterSessionKey() { return KMS; }

  public int getNumActiveSessions() { return sessions.size(); }

  public SMACKServerSession getSession(int ID) {

    return sessions.get(new Integer(ID));

  }

  public void addSession(int ID, byte[] sessionKey) {

    SMACKServerSession s = new SMACKServerSession(ID, sessionKey);
    sessions.put(new Integer(ID), s);

  }

  public void updateMasterSessionKey(byte[] masterSessionKey) { KMS = masterSessionKey; }

  public Object removeSession(int ID) {

    return sessions.remove(new Integer(ID));

  }

}
