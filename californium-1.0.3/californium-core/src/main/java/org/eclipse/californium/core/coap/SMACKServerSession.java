package org.eclipse.californium.SMACKEnvironment;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
//import org.eclipse.californium.coap.EndpointAddress; //Disabled //Rikard
import org.eclipse.californium.coap.SMACKengine.SMACKEngine;
//import org.eclipse.californium.util.Properties; //Disabled //Rikard

public class SMACKServerSession extends SMACKSession {

  private Set<Integer> receivedMessageIDs; // Set of non initial message IDs received during this session so far
  private Map<Integer,byte[]> fragmentSessionKeys; // Current keys used to derive c_i (initialized to KS)
  
  private byte[] createNewKey(int index) {
  
    int i = index - 1;
    int steps = 1;
    
    // Look for already existing keys. In the worst case, the main session key KS will be found at entry 0
    while (!fragmentSessionKeys.containsKey(new Integer(i))) {
    
      i--;
      steps++;
      
    }
  
    // Build all missing keys from the first found to the one just required
    for (int j = 0; j < steps; j++) {
    
      byte[] newKey = SMACKEngine.computeKey(KS, fragmentSessionKeys.get(new Integer(i)));
      i++;
      fragmentSessionKeys.put(new Integer(i), newKey);
    
    }
    
    return fragmentSessionKeys.get(new Integer(index));
  
  }
  
  public SMACKServerSession(int firstID, byte[] sessionKey) {
  
    super(firstID, sessionKey);
    receivedMessageIDs = new HashSet<Integer>();
    fragmentSessionKeys = new HashMap<Integer, byte[]>();
    fragmentSessionKeys.put(new Integer(0), sessionKey);
    
  }
  
  public boolean isOldMessage(int msgID) { return receivedMessageIDs.contains(msgID); }
  
  public void registerMessageID(int msgID) { receivedMessageIDs.add(msgID); }
  
  public boolean usedKey(int index) { return fragmentSessionKeys.containsKey(new Integer(index)); }
  
  public byte[] getKey(int index) { return fragmentSessionKeys.get(new Integer(index)); }
  
  public byte[] addKey(int index) {
  
    byte[] key = createNewKey(index);
    
    // The key has already been inserted
    return key;
    
  }
  
}
