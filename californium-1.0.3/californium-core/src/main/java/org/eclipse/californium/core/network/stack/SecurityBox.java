package org.eclipse.californium.core.network.stack;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Logger;
import java.util.Arrays;
import java.nio.ByteBuffer;
import org.apache.commons.lang.builder.ToStringBuilder; // debugging purpose //Syafiq
import org.apache.commons.lang.builder.ToStringStyle; // debugging purpose //Syafiq
import java.util.Timer;
import java.util.TimerTask;
import java.util.Date;
import java.lang.Math;
import java.util.List; // syafiq
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.CacheStats;
import com.google.common.cache.LoadingCache;
import com.google.common.primitives.Ints;

import org.eclipse.californium.scandium.config.DtlsConnectorConfig;
import org.eclipse.californium.scandium.config.DtlsConnectorConfig.Builder;
import org.eclipse.californium.scandium.DTLSConnector;
import org.eclipse.californium.scandium.dtls.pskstore.StaticPskStore;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.coap.Request;
import org.eclipse.californium.core.coap.Response;
import org.eclipse.californium.core.coap.CoAP.Code;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.SMACKEnvironment.SMACKClientEnvironment;
import org.eclipse.californium.core.network.stack.SMACKLayer;
import org.eclipse.californium.core.coap.CoAP.Type;
import java.net.URLDecoder;
import java.io.UnsupportedEncodingException;
import org.eclipse.californium.core.coap.OptionSet;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;

import org.eclipse.californium.core.network.Exchange; // Syafiq
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.eclipse.californium.core.network.EndpointManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import org.joda.time.*;

public class SecurityBox {
  private static NetworkConfig config = NetworkConfig.getStandard();
  private static boolean isServer = config.getBoolean(NetworkConfig.Keys.IS_SERVER);
  private static boolean isClient = config.getBoolean(NetworkConfig.Keys.IS_CLIENT);
  private static boolean isProxy  = config.getBoolean(NetworkConfig.Keys.IS_PROXY);
	private static boolean SMACKActive = config.getBoolean(NetworkConfig.Keys.SMACK_ACTIVE);

  private final static Logger LOGGER = Logger.getLogger(SecurityBox.class.getCanonicalName()); // syafiq

  // Server variable
  // ======================================================
  // indices of command line parameters
  private static final int IDX_METHOD          = 0;
  private static final int IDX_URI             = 1;
  private static final int IDX_PAYLOAD         = 2;
  // exit codes for runtime errors
  private static final int ERR_MISSING_METHOD  = 1;
  private static final int ERR_UNKNOWN_METHOD  = 2;
  private static final int ERR_MISSING_URI     = 3;
  private static final int ERR_BAD_URI         = 4;
  private static final int ERR_REQUEST_FAILED  = 5;
  private static final int ERR_RESPONSE_FAILED = 6;
  private static final int ERR_BAD_LINK_FORMAT = 7;
  private static String identity = "Client_identity";
  private static String psk = "secretPSK";
  public static CoapClient ServerToKDC;
  public static CoapClient CachedMessageProcessing;
  public static CoapResponse responseKDCToServer;
  public static CoapEndpoint ServerDTLSEndpoint;
  public static Response receivedOffResponse;

  private static String ClientAddr = config.getString(NetworkConfig.Keys.CLIENT_ADDRESS);
	private static String ProxyAddr  = config.getString(NetworkConfig.Keys.PROXY_ADDRESS);
	private static String ServerAddr = config.getString(NetworkConfig.Keys.SERVER_ADDRESS);
  private static int ProxySecurePort = config.getInt(NetworkConfig.Keys.PROXY_SECURE_PORT);
  private static int ServerSecurePort = config.getInt(NetworkConfig.Keys.SERVER_SECURE_PORT);
  private static int ServerPort = config.getInt(NetworkConfig.Keys.SERVER_PORT);

  public static int Th0_MAX = config.getInt(NetworkConfig.Keys.Th0_MAX);
  public static int Th0_MIN = config.getInt(NetworkConfig.Keys.Th0_MIN);
  public static int t0 = config.getInt(NetworkConfig.Keys.t0);
  public static int Th1_MAX = config.getInt(NetworkConfig.Keys.Th1_MAX);
  public static int Th1_MIN = config.getInt(NetworkConfig.Keys.Th1_MIN);
  public static int t1 = config.getInt(NetworkConfig.Keys.t1);
  public static int T1 = config.getInt(NetworkConfig.Keys.T1);
  public static int T2_MIN = config.getInt(NetworkConfig.Keys.T2_MIN);
  public static int T2_MAX = config.getInt(NetworkConfig.Keys.T2_MAX);

  public static int C;
  public static int Th0;
  public static int W0=t0;
  public static int Th1;
  public static int W1=t1;
  public static int T2;

  public static Timer timer0;
  public static Timer timer1;
  public static Timer timer2;
  public static Timer TimerT1;
  public static Timer TimerT2;

  // Proxy Variable
  // ======================================================

  // Client variable
  // ======================================================
  public static byte[] ClientSessionKey;
  public static int SessionID;

  private static byte[] State_NP_ServerToClient = new byte[] { 0x01, 0x00, };
	private static byte[] State_NP_ServerToKDC 	 	= new byte[] { 0x01, 0x01, };
	private static byte[] State_NP_KDCToServer 	 	= new byte[] { 0x01, 0x02, };
	private static byte[] State_PO_ServerToKDC 	 	= new byte[] { 0x01, 0x03, };
	private static byte[] State_PO_KDCToServer 	 	= new byte[] { 0x01, 0x04, };
	private static byte[] State_PO_KDCToClient		= new byte[] { 0x01, 0x05, };
	private static byte[] State_OP_ServerToKDC 	 	= new byte[] { 0x01, 0x06, };
	private static byte[] State_OP_KDCToServer 	 	= new byte[] { 0x01, 0x07, };
	private static byte[] State_OP_KDCToClient 	 	= new byte[] { 0x01, 0x08, };
	private static byte[] State_PN_ServerToKDC 	 	= new byte[] { 0x01, 0x09, };
	private static byte[] State_PN_KDCToServer 	 	= new byte[] { 0x01, 0x0a, };
	private static byte[] State_PN_KDCToClient 	 	= new byte[] { 0x01, 0x0b, };

  public static boolean isServerNormal;
  public static boolean isServerProtected;
  public static boolean isServerOff;
  public static boolean isProxyNormal;
  public static boolean isProxyProtected;
  public static boolean isProxyOff;
  public static boolean isClientNormal;
  public static boolean isClientProtected;
  public static boolean isClientOff;

  public static CoapClient ClientToKDC;
  public static CoapResponse responseKDCToClient;
  public static CoapEndpoint ClientDTLSEndpoint;
  public static Request incomingOffRequest;
  //public static Request outgoingOffRequest;
	public static Request stateOffRequest;
  public static Exchange incomingOffExchange;

  public static int ReceivedTimeOff;

  private static final long CACHE_SIZE = NetworkConfig.getStandard().getInt(NetworkConfig.Keys.HTTP_CACHE_SIZE);

	//public static CoapEndpoint secureEndpointSecbox; //Secure endpoint for DTLS //Rikard

  public static Cache<String, Request> cacheIncomingReq = CacheBuilder.newBuilder().maximumSize(CACHE_SIZE).build();
  public static Cache<String, Request> cacheOutgoingReq = CacheBuilder.newBuilder().maximumSize(CACHE_SIZE).build();
  public static Cache<String, Exchange> cacheIncomingExchange = CacheBuilder.newBuilder().maximumSize(CACHE_SIZE).build();

	public static List<String> TokenList = new ArrayList<String>();

  public static List<Exchange> listExchange = new ArrayList<Exchange>();

  private static int securePort= 5684; //Default port DTLS listener runs on //Rikard

  //InetSocketAddress bindToAddress = new InetSocketAddress(ProxyAddr, securePort);
  public static Builder builder = new DtlsConnectorConfig.Builder(new InetSocketAddress(ProxyAddr, ProxySecurePort)).setPskStore(new StaticPskStore(identity, psk.getBytes()));
  public static CoapEndpoint secureEndpointSecbox = new CoapEndpoint(new DTLSConnector(builder.build()), NetworkConfig.getStandard(), false);

  public static Builder builderServerSide = new DtlsConnectorConfig.Builder(new InetSocketAddress(ProxyAddr, 5694)).setPskStore(new StaticPskStore(identity, psk.getBytes()));
  public static CoapEndpoint secureEndpointSecboxServerSide = new CoapEndpoint(new DTLSConnector(builderServerSide.build()), NetworkConfig.getStandard(), false);

  public static DateTime startOffTime;
  public static DateTime endOffTime;
  public static DateTime currentTime;
  public static int k;
  public static int[] decr = {1, 2, 4, 8, 16, 32, 64, 128, 256};

  // add smacklayer reference here // note rikard
  public SecurityBox() {
    if(isProxy && !isServer && !isClient){
      runProxy(); // should never be invoked, since proxy does not trigger SMACKLayer
    } else if(isServer && !isProxy && !isClient) {
      runServer();
    } else if(isClient && !isProxy && !isServer) {
      runClient();
    } else {
      System.out.println("Undefined Security Box");
    }
  }

  // API to be accessed by other layer
  public static void InvalidCounterIncrement(){
    C = C+1;
  }

  // to
  public static void TimerPhase_0() {
    timer0 = new Timer(true);
    timer0.schedule(new TimerTask() {
        @Override
        public void run() {
          String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
          System.out.println(""+timeStamp+" counter= "+C+" threshold= "+Th0+" Phase= 0");
          if (C < Th0_MIN) { // Negligible or no impact
            Th0 = Math.min(2*Th0, Th0_MAX);
            ResetCounter();
          } else if (C < Th0 && C >= Th0_MIN) { // light impact
            Th0 = Math.max(Th0/2, Th0_MIN);
            ResetCounter();
          } else if (C > Th0) {
            //ResetCounter();
            setServerStateProtected();
          }
        }
    }, W0*1000, W0*1000);
  }

  // t1
  public static void TimerPhase_1() {
    timer1 = new Timer(true);
    timer1.schedule(new TimerTask() {
        @Override
        public void run() {
          String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
          System.out.println(""+timeStamp+" counter= "+C+" threshold= "+Th1+" Phase= 1");
          if (C < Th1_MIN) { // should back to normal state
            Th1 = Math.min(2*Th1, Th1_MAX);
            ResetCounter();
          } else if (C < Th1 && C >= Th1_MIN) { // light impact
            //Th1 = Math.max(Th1/2, Th1_MIN); // changed to below (after discussion with Marco)
            Th1 = Math.min(Th1-decr[k], Th1_MAX);
            k = k+1;
            ResetCounter();
          } else if (C > Th1) {
            //ResetCounter();
            setServerStateOff();
          }
		}
    }, W1*1000, W1*1000);
  }

  // T1
  public static void StartTimer_T1() {
    TimerT1 = new Timer(true);
    TimerT1.schedule(new TimerTask() {
        @Override
        public void run() {
          setServerStateNormal();
        }
    }, T1*1000, T1*1000);
  }

  // T2
  public static void StartTimer_T2(int T2) {
    TimerT2 = new Timer(true);
    TimerT2.schedule(new TimerTask() {
        @Override
        public void run() {
          setServerStateProtected();
          ResetCounter();
        }
    }, T2*1000, T2*1000);
  }

  // set server state to NORMAL
  public static void setServerStateNormal() {
    String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
	System.out.println(""+timeStamp+" STATE SERVER: NORMAL");
	// previous state
    if (!isServerNormal && !isServerProtected && !isServerOff) { // initialization
      Th0 = Th0_MAX;
    } else if (!isServerNormal && isServerProtected && !isServerOff) { // from protected state
      TimerT1.cancel();
      timer1.cancel();
      Th0 = Th0_MIN;
      PNNotifKDC();
    }

		isServerNormal = true;
		isServerProtected = false;
		isServerOff = false;
    TimerPhase_0();
	}

  // set server state to PROTECTED
	public static void setServerStateProtected() {
    k=0;
	// previous state
	  String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
    System.out.println(""+timeStamp+" STATE SERVER: PROTECTED");
	  if (isServerNormal && !isServerProtected && !isServerOff) { // from normal state
      ResetCounter();
      timer0.cancel();
      NPNotifKDC();
    } else if (!isServerNormal && !isServerProtected && isServerOff) { // from off state
      ResetCounter();
      TimerT2.cancel();

      try {
        Runtime.getRuntime().exec("ifconfig eth0:0 10.99.99.88 netmask 255.255.255.0 up");
      } catch (IOException e) {
        e.printStackTrace();
      }

      OPNotifKDC();
    }

    //Th1 = Th1_MIN; // changed to below (after discussion with Marco)
    Th1 = Th1_MAX;
		isServerNormal = false;
		isServerProtected = true;
		isServerOff = false;
    TimerPhase_1();
    StartTimer_T1();
	}

  // set server state to OFF
	public static void setServerStateOff() {
    String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
	  System.out.println(""+timeStamp+" STATE SERVER: OFF");
	  // previous state
    //if (!isServerNormal && isServerProtected && !isServerOff) { // from protected state

    TimerT1.cancel();
    timer1.cancel();
    PONotifKDC();
    //}

		isServerNormal = false;
		isServerProtected = false;
		isServerOff = true;
    try {
      Runtime.getRuntime().exec("ifconfig eth0:0 down");
    } catch (IOException e) {
      e.printStackTrace();
    }

    Random random = new Random();
    T2 = random.nextInt(T2_MAX - T2_MIN + 1) + T2_MIN;
    System.out.println("T2 = "+T2);
    StartTimer_T2(T2);
	}

  public static boolean getIsServerNormal() {
    return isServerNormal;
  }

  public static boolean getIsServerProtected() {
    return isServerProtected;
  }

  public static boolean getIsServerOff() {
    return isServerOff;
  }

  public static int getCounterValue() {
    return C;
  }

  public static void ResetCounter() {
    C = 0;
  }

  public static void setClientStateNormal() {
		isClientNormal = true;
		isClientProtected = false;
		isClientOff = false;
	}

	public static void setClientStateProtected() {
		isClientNormal = false;
		isClientProtected = true;
		isClientOff = false;
	}

	public static void setClientStateOff() {
		isClientNormal = false;
		isClientProtected = false;
		isClientOff = true;
	}

  public static boolean getIsClientNormal() {
    return isClientNormal;
  }

  public static boolean getIsClientProtected() {
    return isClientProtected;
  }

  public static boolean getIsClientOff() {
    return isClientOff;
  }

  public static void setProxyStateNormal() {
		isProxyNormal = true;
		isProxyProtected = false;
		isProxyOff = false;
	}

	public static void setProxyStateProtected() {
		isProxyNormal = false;
		isProxyProtected = true;
		isProxyOff = false;
	}

  // send ALL cached request on the proxy
  // accessed by proxy apps when the OFF state is over
  public static void sendCached() {
    //int i = listExchange.size()-1;
    //int i = 0;
    for (Exchange x:listExchange) {
      sendCachedRequest(x);
      try {
          Thread.sleep(50);
      } catch(InterruptedException ex) {
          Thread.currentThread().interrupt();
      }
    }
    //while (listExchange.get(i) != null) {
    //  //sendCachedRequest(listExchange.get(i));
    //  i=i++;
    //}
    listExchange.clear();
  }

  // send ONE cached request on the proxy
  // accessed by proxy apps when the OFF state is over
  public static void sendCachedRequest(Exchange exchange) {
    if (!listExchange.isEmpty() && isProxy) {
        incomingOffRequest = exchange.getRequest();
        //System.out.println("incomingOffRequest ="+incomingOffRequest);
        incomingOffRequest.getOptions().clearUriPath();
        Request outgoingOffRequest = new Request(Code.GET);
        try {
          outgoingOffRequest = translateRequest(incomingOffRequest);
          //secureEndpointSecbox.sendRequestCached(outgoingOffRequest);
        } catch (TranslationException e) {
            LOGGER.warning("Proxy-uri option malformed: " + e.getMessage());
        }
        outgoingOffRequest.setToken(incomingOffRequest.getToken());
        //outgoingOffRequest.setMID(incomingOffRequest.getMID());
        outgoingOffRequest.setPayload((byte[]) null);
        System.out.println("outgoingOffRequest ="+outgoingOffRequest);
        //outgoingOffRequest.send(secureEndpointSecbox);
        //secureEndpointSecbox.sendRequestCached(outgoingOffRequest);

        if (!secureEndpointSecboxServerSide.isStarted()) {
          try{
            secureEndpointSecboxServerSide.start();
          } catch (IOException e) {
            System.out.println("start endpoint failed");
          }
        }

        try {
          System.out.println("inside received off response");
    			receivedOffResponse = outgoingOffRequest.send(secureEndpointSecboxServerSide).waitForResponse(50000);
    		} catch (Exception e) {
    			System.err.println("Failed to receive response: " + e.getMessage());
    			System.exit(ERR_RESPONSE_FAILED);
    		}

        System.out.println("receivedOffResponse ="+receivedOffResponse);

        if (receivedOffResponse != null) {
          System.out.println("Coap response received.");
          receivedOffResponse.setDestination(incomingOffRequest.getSource());
          //System.out.println("rcv resp dest ="+receivedOffResponse.getDestination());
          exchange.sendResponse(receivedOffResponse);
          //super.sendResponse(receivedOffResponse);
          // create the real response for the original request
          // Response outgoingOffResponse = translateResponse(receivedOffResponse);
          // System.out.println("outgoingOffResponse = "+outgoingOffResponse);
        } else {
          LOGGER.warning("No response received.");
          //return new Response(CoapTranslator.STATUS_TIMEOUT);
        }

        //Thread.sleep(1000);
        //try {
        //  // receive the response // TODO: don't wait for ever
        //  receivedOffResponse = outgoingOffRequest.waitForResponse();
        //  System.out.println("receivedOffResponse ="+receivedOffResponse);

        //  if (receivedOffResponse != null) {
        //    System.out.println("Coap response received.");
        //    receivedOffResponse.setDestination(incomingOffRequest.getSource());
        //    System.out.println("rcv resp dest ="+receivedOffResponse.getDestination());
        //    object.sendResponse(receivedOffResponse);
        //    //super.sendResponse(receivedOffResponse);
        //    // create the real response for the original request
        //    // Response outgoingOffResponse = translateResponse(receivedOffResponse);
        //    // System.out.println("outgoingOffResponse = "+outgoingOffResponse);
        //  } else {
        //    LOGGER.warning("No response received.");
        //    //return new Response(CoapTranslator.STATUS_TIMEOUT);
        //  }
        //} catch (InterruptedException e) {
        //  LOGGER.warning("Receiving of response interrupted: " + e.getMessage());
        //  //return new Response(ResponseCode.INTERNAL_SERVER_ERROR);
        //}
    }
  }

  //@Override
  //public void sendResponse(Exchange exchange, Response response) {
  //  exchange.sendResponse(response);
  //}

	public static void setProxyStateOff() {
		isProxyNormal = false;
		isProxyProtected = false;
		isProxyOff = true;
	}

  public static boolean getIsProxyNormal() {
    return isProxyNormal;
  }

  public static boolean getIsProxyProtected() {
    return isProxyProtected;
  }

  public static boolean getIsProxyOff() {
    return isProxyOff;
  }

  // off time information from the server to the proxy
  public static void setReceivedTimeOff(int RTO) {
    ReceivedTimeOff = RTO;
    startOffTime = new DateTime();
    endOffTime = startOffTime.plusSeconds(ReceivedTimeOff);
  }

  public static int getReceivedTimeOff() {
    return ReceivedTimeOff;
  }

  // off time must ALWAYS be compared to local time of the proxy
  public static Period getRemainingOffTime() {
    currentTime = new DateTime();
    Period p = new Period(currentTime, endOffTime);
    System.out.println("period p ="+p.getSeconds());
    return p;
  }

  public void runProxy(){ // should never be invoked
    System.out.println("Proxy is running!");
  }

  // running the server for the first time -> contact KDC
  public void runServer() {
    setServerStateNormal();
    LOGGER.info(String.format("SERVER : Start server request to KDC"));
    URI uriServerKDC = null;
    String InputUriServerKDC = "coaps://"+ProxyAddr+":"+ProxySecurePort+"/smack";
    try {
      uriServerKDC = new URI(InputUriServerKDC);
    } catch (URISyntaxException e) {
      System.err.println("Failed to parse URI: " + e.getMessage());
      System.exit(ERR_BAD_URI);
    }
    ServerToKDC = new CoapClient(uriServerKDC);

    DtlsConnectorConfig.Builder builder = new DtlsConnectorConfig.Builder(new InetSocketAddress(ServerAddr, ServerSecurePort));
    builder.setPskStore(new StaticPskStore(identity, psk.getBytes()));
    ServerDTLSEndpoint = new CoapEndpoint(new DTLSConnector(builder.build()), NetworkConfig.getStandard(), false);
    //ServerToKDC.setEndpoint(new CoapEndpoint(new DTLSConnector(builder.build()), NetworkConfig.getStandard(), false));
    ServerToKDC.setEndpoint(ServerDTLSEndpoint);
    ServerToKDC.setTimeout(0); // add syafiq
	// Request builder
    byte[] payloadServerToKDC = new byte[] { 0x00, 0x00, };
    Request ReqServerToKDC = new Request(Code.GET);
    ReqServerToKDC.setPayload((byte[]) payloadServerToKDC);

    //responseKDCToServer = ServerToKDC.get(); // change to below
    responseKDCToServer = ServerToKDC.advanced(ReqServerToKDC);

    if (responseKDCToServer != null) {
    	System.out.println(responseKDCToServer.getPayload());
    	byte[] payloadKDCToServer = responseKDCToServer.getPayload();
      byte[] SMACKKDCToServer = Arrays.copyOfRange(payloadKDCToServer, 0, 2);
    	byte[] masterKey = Arrays.copyOfRange(payloadKDCToServer, 2, 34);
    	byte[] seed = Arrays.copyOfRange(payloadKDCToServer, 34, 66);
      System.out.println("SMACKKDCToServer ="+ToStringBuilder.reflectionToString(SMACKKDCToServer));
    	System.out.println("masterKey ="+ToStringBuilder.reflectionToString(masterKey));
    	System.out.println("seed ="+ToStringBuilder.reflectionToString(seed));
      SMACKLayer.setSMACKServerEnvironment(masterKey, seed);
    } else {
    	System.out.println("No response received.");
    }
  }

  // running the server after SMACK material is obtained
  public static void runServerInitCont() {
    // Request builder
    byte[] payloadServerToKDC = new byte[] { 0x00, 0x04, };
    Request ReqServerToKDC = new Request(Code.GET);
    ReqServerToKDC.setPayload((byte[]) payloadServerToKDC);

    responseKDCToServer = ServerToKDC.advanced(ReqServerToKDC);

    if (responseKDCToServer != null) {
      System.out.println(responseKDCToServer.getPayload());
      byte[] payloadKDCToServer = responseKDCToServer.getPayload();
      byte[] SMACKKDCToServer = Arrays.copyOfRange(payloadKDCToServer, 0, 2);
      byte[] masterKey = Arrays.copyOfRange(payloadKDCToServer, 2, 34);
      byte[] seed = Arrays.copyOfRange(payloadKDCToServer, 34, 66);
      System.out.println("SMACKKDCToServer ="+ToStringBuilder.reflectionToString(SMACKKDCToServer));
      System.out.println("masterKey ="+ToStringBuilder.reflectionToString(masterKey));
      System.out.println("seed ="+ToStringBuilder.reflectionToString(seed));
      SMACKLayer.setSMACKServerEnvironment(masterKey, seed);
    } else {
      System.out.println("No response received.");
    }
  }

  // running the client for the first time -> contact KDC
  public void runClient(){
    setClientStateNormal();
    LOGGER.info(String.format("CLIENT : Start client request to KDC"));
    URI uriClientKDC = null;
    String InputUriClientKDC = "coaps://"+ProxyAddr+":"+ProxySecurePort+"/smack"; // should be dynamically input by user in the future //fiq
    try {
      uriClientKDC = new URI(InputUriClientKDC);
    } catch (URISyntaxException e) {
      System.err.println("Failed to parse URI: " + e.getMessage());
      System.exit(ERR_BAD_URI);
    }
    ClientToKDC = new CoapClient(uriClientKDC);

    // ==
    DTLSConnector dtlsConnector;
    DtlsConnectorConfig.Builder builder = new DtlsConnectorConfig.Builder(new InetSocketAddress(ClientAddr,0));
    builder.setPskStore(new StaticPskStore(identity, psk.getBytes()));
    dtlsConnector = new DTLSConnector(builder.build(), null);
    ClientDTLSEndpoint = new CoapEndpoint(dtlsConnector, NetworkConfig.getStandard(), false);
    //ClientToKDC.setEndpoint(new CoapEndpoint(dtlsConnector, NetworkConfig.getStandard(), false));
    ClientToKDC.setEndpoint(ClientDTLSEndpoint);

    LOGGER.info(String.format("Performing CoAP request to " + uriClientKDC.toString()));
    if(uriClientKDC.toString().contains("coaps"))
      System.out.println("INFO: Connection is using DTLS with ID '" + identity + "' and Key '" + psk + "'.");

    // Request builder
    byte[] payloadClientToKDC = new byte[] { 0x00, 0x02, };
    Request ReqClientToKDC = new Request(Code.GET);
    ReqClientToKDC.setPayload((byte[]) payloadClientToKDC);

    //responseKDCToClient = ClientToKDC.get(); // change to below
    responseKDCToClient = ClientToKDC.advanced(ReqClientToKDC);

		if (responseKDCToClient != null) {
      System.out.println(responseKDCToClient.getPayload());
      byte[] payloadKDCToClient = (responseKDCToClient.getPayload());
      System.out.println("Payload ="+ToStringBuilder.reflectionToString(payloadKDCToClient));
      if(SMACKActive) {
        byte[] SMACKKDCToServer = Arrays.copyOfRange(payloadKDCToClient, 0, 2);
        byte[] SMACKSessionKey = Arrays.copyOfRange(payloadKDCToClient, 2, 34);
        byte[] ArrayCurrentSessionID = Arrays.copyOfRange(payloadKDCToClient, 34, 38);
        Integer currentSessionID = ByteBuffer.wrap(ArrayCurrentSessionID).getInt();
        setClientSessionKey(SMACKSessionKey);
        setCurrentSessionID(currentSessionID);
        System.out.println("SMACKSessionKey ="+ToStringBuilder.reflectionToString(SMACKSessionKey));
        System.out.println("ArrayCurrentSessionID ="+ToStringBuilder.reflectionToString(ArrayCurrentSessionID));
        System.out.println("currentSessionID ="+currentSessionID);
        //SMACKClientEnvironment environment = new SMACKClientEnvironment();
        //SMACKClientEnvironment.addSession(currentSessionID, SMACKSessionKey);
        //request.setMID(currentSessionID);
      }
    } else {
	    System.out.println("No response received.");
    }
  }

  // running the client after SMACK material is obtained
  public static void runClientInitCont(){
    // Request builder
    byte[] payloadClientToKDC = new byte[] { 0x00, 0x02, };
    Request ReqClientToKDC = new Request(Code.GET);
    ReqClientToKDC.setPayload((byte[]) payloadClientToKDC);

    //responseKDCToClient = ClientToKDC.get(); // change to below
    responseKDCToClient = ClientToKDC.advanced(ReqClientToKDC);

		if (responseKDCToClient != null) {
      System.out.println(responseKDCToClient.getPayload());
      byte[] payloadKDCToClient = (responseKDCToClient.getPayload());
      System.out.println("Payload ="+ToStringBuilder.reflectionToString(payloadKDCToClient));
      if(SMACKActive) {
        byte[] SMACKKDCToServer = Arrays.copyOfRange(payloadKDCToClient, 0, 2);
        byte[] SMACKSessionKey = Arrays.copyOfRange(payloadKDCToClient, 2, 34);
        byte[] ArrayCurrentSessionID = Arrays.copyOfRange(payloadKDCToClient, 34, 38);
        Integer currentSessionID = ByteBuffer.wrap(ArrayCurrentSessionID).getInt();
        setClientSessionKey(SMACKSessionKey);
        setCurrentSessionID(currentSessionID);
        System.out.println("SMACKSessionKey ="+ToStringBuilder.reflectionToString(SMACKSessionKey));
        System.out.println("ArrayCurrentSessionID ="+ToStringBuilder.reflectionToString(ArrayCurrentSessionID));
        System.out.println("currentSessionID ="+currentSessionID);
        //SMACKClientEnvironment environment = new SMACKClientEnvironment();
        //SMACKClientEnvironment.addSession(currentSessionID, SMACKSessionKey);
        //request.setMID(currentSessionID);
      }
    } else {
	    System.out.println("No response received.");
    }
  }

  public static void setClientSessionKey(byte[] InputSessionKey){
    ClientSessionKey = InputSessionKey;
  }

  public byte[] getClientSessionKey(){
    return ClientSessionKey;
  }

  public static void setCurrentSessionID(int InputCurrentSessionID) {
    SessionID = InputCurrentSessionID;
  }

  public int getCurrentSessionID() {
    return SessionID;
  }

  // Inform the KDC that server changes its operative state from Normal to Protected.
  public static void NPNotifKDC() {
		URI uriStateServerKDC = null;
		String InputUriStateServerKDC = "coaps://"+ProxyAddr+":"+ProxySecurePort+"/state";
		try {
			uriStateServerKDC = new URI(InputUriStateServerKDC);
		} catch (URISyntaxException e) {
			System.err.println("Failed to parse URI: " + e.getMessage());
			System.exit(ERR_BAD_URI);
		}

		CoapClient NPServerToKDC = new CoapClient(uriStateServerKDC);
		CoapResponse responseNPKDCToServer;
    NPServerToKDC.setEndpoint(ServerDTLSEndpoint);

		Request NPReqServerToKDC = new Request(Code.GET);
		NPReqServerToKDC.setPayload((byte[]) State_NP_ServerToKDC);

		responseNPKDCToServer = NPServerToKDC.advanced(NPReqServerToKDC);

		if (responseNPKDCToServer != null) {
			System.out.println("received ok from KDC to act as relay! ="+ToStringBuilder.reflectionToString(responseNPKDCToServer.getPayload()));
			//System.out.println(responseNPKDCToServer.getPayload());
		} else {
			System.out.println("No response received.");
		}
	}

  // Inform the KDC that server is come back to Normal from Protected.
  public static void PNNotifKDC() {
    URI uriStateServerKDC = null;
    String InputUriStateServerKDC = "coaps://"+ProxyAddr+":"+ProxySecurePort+"/state";
    try {
      uriStateServerKDC = new URI(InputUriStateServerKDC);
    } catch (URISyntaxException e) {
      System.err.println("Failed to parse URI: " + e.getMessage());
      System.exit(ERR_BAD_URI);
    }

    CoapClient PNServerToKDC = new CoapClient(uriStateServerKDC);
    CoapResponse responsePNKDCToServer;

    PNServerToKDC.setEndpoint(ServerDTLSEndpoint);

    // Request builder
    Request PNReqServerToKDC = new Request(Code.GET);
    PNReqServerToKDC.setPayload((byte[]) State_PN_ServerToKDC);
    responsePNKDCToServer = PNServerToKDC.advanced(PNReqServerToKDC);

    if (responsePNKDCToServer != null) {
      System.out.println("received ok from KDC to back to normal! ="+ToStringBuilder.reflectionToString(responsePNKDCToServer.getPayload()));
    } else {
      System.out.println("No response received.");
    }
  }

  // Inform the KDC that server changes its operative state from Protected to Off.
  public static void PONotifKDC() {
    URI uriStateServerKDC = null;
    String InputUriStateServerKDC = "coaps://"+ProxyAddr+":"+ProxySecurePort+"/state";
    try {
      uriStateServerKDC = new URI(InputUriStateServerKDC);
    } catch (URISyntaxException e) {
      System.err.println("Failed to parse URI: " + e.getMessage());
      System.exit(ERR_BAD_URI);
    }

    CoapClient POServerToKDC = new CoapClient(uriStateServerKDC);
    POServerToKDC.useNONs();
	CoapResponse responsePOKDCToServer;
    POServerToKDC.setEndpoint(ServerDTLSEndpoint);

    Request POReqServerToKDC = new Request(Code.GET);
    byte[] OffTimeServer  = ByteBuffer.allocate(4).putInt(T2).array();
    byte[] payloadOffTime = new byte[State_PO_ServerToKDC.length + OffTimeServer.length];
    System.arraycopy(State_PO_ServerToKDC, 0, payloadOffTime, 0, State_PO_ServerToKDC.length);
    System.arraycopy(OffTimeServer, 0, payloadOffTime, State_PO_ServerToKDC.length, OffTimeServer.length);
    POReqServerToKDC.setPayload(payloadOffTime);

    responsePOKDCToServer = POServerToKDC.advanced(POReqServerToKDC);

    if (responsePOKDCToServer != null) {
      System.out.println("received ok from KDC to do caching! ="+ToStringBuilder.reflectionToString(responsePOKDCToServer.getPayload()));
      //System.out.println(responseNPKDCToServer.getPayload());
    } else {
      System.out.println("No response received.");
    }
  }

  // nform the KDC that server is come back to Protected from Off.
  public static void OPNotifKDC() {
    URI uriStateServerKDC = null;
    String InputUriStateServerKDC = "coaps://"+ProxyAddr+":"+ProxySecurePort+"/state";
    try {
      uriStateServerKDC = new URI(InputUriStateServerKDC);
    } catch (URISyntaxException e) {
      System.err.println("Failed to parse URI: " + e.getMessage());
      System.exit(ERR_BAD_URI);
    }

    CoapClient OPServerToKDC = new CoapClient(uriStateServerKDC);
    CoapResponse responseOPKDCToServer;
    OPServerToKDC.setEndpoint(ServerDTLSEndpoint);

    Request OPReqServerToKDC = new Request(Code.GET);
    OPReqServerToKDC.setPayload((byte[]) State_OP_ServerToKDC);
    //OPReqServerToKDC.setType(Type.NON);

    responseOPKDCToServer = OPServerToKDC.advanced(OPReqServerToKDC);

    if (responseOPKDCToServer != null) {
      System.out.println("received ok from KDC to go back to PROTECTED mode! ="+ToStringBuilder.reflectionToString(responseOPKDCToServer.getPayload()));
      //System.out.println(responseNPKDCToServer.getPayload());
    } else {
      System.out.println("No response received OFF-PROTECTED.");
    }
  }

  // not used anymore
  public static Request translateRequest(final Request incomingRequest) throws TranslationException {
    // check parameters
    if (incomingRequest == null) {
      throw new IllegalArgumentException("incomingRequest == null");
    }

    // get the code
    Code code = incomingRequest.getCode();

    // get message type
    Type type = incomingRequest.getType();

    // create the request
    Request outgoingRequest = new Request(code);
    outgoingRequest.setConfirmable(type == Type.CON);

    // copy payload
    byte[] payload = incomingRequest.getPayload();
    outgoingRequest.setPayload(payload);

    // get the uri address from the proxy-uri option
    URI serverUri;
    try {
      /*
       * The new draft (14) only allows one proxy-uri option. Thus, this
       * code segment has changed.
       */
      String proxyUriString = URLDecoder.decode(
          incomingRequest.getOptions().getProxyUri(), "UTF-8");
      serverUri = new URI(proxyUriString);
    } catch (UnsupportedEncodingException e) {
      LOGGER.warning("UTF-8 do not support this encoding: " + e);
      throw new TranslationException("UTF-8 do not support this encoding", e);
    } catch (URISyntaxException e) {
      LOGGER.warning("Cannot translate the server uri" + e);
      throw new TranslationException("Cannot translate the server uri", e);
    }

    // copy every option from the original message
    // do not copy the proxy-uri option because it is not necessary in
    // the new message
    // do not copy the token option because it is a local option and
    // have to be assigned by the proper layer
    // do not copy the block* option because it is a local option and
    // have to be assigned by the proper layer
    // do not copy the uri-* options because they are already filled in
    // the new message
    OptionSet options = new OptionSet(incomingRequest.getOptions());
    options.removeProxyUri();
    options.removeBlock1();
    options.removeBlock2();
    options.clearUriPath();
    options.clearUriQuery();
    outgoingRequest.setOptions(options);

    // set the proxy-uri as the outgoing uri
    if (serverUri != null) {
      outgoingRequest.setURI(serverUri);
    }

    LOGGER.finer("Incoming request translated correctly");
    return outgoingRequest;
  }

  // not used anymore
  public static Response translateResponse(final Response incomingResponse) {
		if (incomingResponse == null) {
			throw new IllegalArgumentException("incomingResponse == null");
		}

		// get the status
		ResponseCode status = incomingResponse.getCode();

		// create the response
		Response outgoingResponse = new Response(status);

		// copy payload
		byte[] payload = incomingResponse.getPayload();
		outgoingResponse.setPayload(payload);

		// copy the timestamp
		long timestamp = incomingResponse.getTimestamp();
		outgoingResponse.setTimestamp(timestamp);

		// copy every option
		outgoingResponse.setOptions(new OptionSet(
				incomingResponse.getOptions()));

		LOGGER.finer("Incoming response translated correctly");
		return outgoingResponse;
	}
}
