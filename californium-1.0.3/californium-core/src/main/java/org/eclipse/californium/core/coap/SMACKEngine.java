package org.eclipse.californium.coap.SMACKengine;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.nio.ByteBuffer;
import java.lang.Math;
import java.util.Arrays;
import org.eclipse.californium.scandium.dtls.Handshaker;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.core.coap.Message;
import org.eclipse.californium.core.coap.Response;
import org.eclipse.californium.scandium.util.ByteArrayUtils;
import org.eclipse.californium.core.coap.OptionNumberRegistry;
import org.eclipse.californium.core.network.serialization.DataSerializer; //Rikard
import org.eclipse.californium.core.coap.Response; //Rikard
import org.eclipse.californium.core.coap.Request; //Rikard

import org.eclipse.californium.scandium.util.GaloisField; //Rikard

public class SMACKEngine {

  private static NetworkConfig config = NetworkConfig.getStandard(); //Rikard

  private static GaloisField GF = GaloisField.getInstance(65536, 69643);

  private static DataSerializer serializer = new DataSerializer(); //For getting bytes of a message //Rikard

  public static byte[] PRF(byte[] secret, byte[] data) {
	  /*
	    * P_hash(secret, seed) = HMAC_hash(secret, A(1) + seed) +
	    * HMAC_hash(secret, A(2) + seed) + HMAC_hash(secret, A(3) + seed) + ...
	    * where + indicates concatenation. A() is defined as: A(0) = seed, A(i)
	    * = HMAC_hash(secret, A(i-1))
	    */

	  double hashLength = 0;
	  int length = config.getInt(NetworkConfig.Keys.SMACK_KEY_SIZE);
	  String digestAlgorithm = config.getString(NetworkConfig.Keys.SMACK_HMAC);

	  switch(digestAlgorithm) {
	    case "MD2":
	      hashLength = 16;
	      break;
	    case "MD5":
	      hashLength = 16;
	      break;
	    case "SHA-1":
	      hashLength = 20;
	      break;
	    case "SHA-256":
	      hashLength = 32;
	      break;
	    case "SHA-384":
	      hashLength = 48;
	      break;
	    case "SHA-512":
	      hashLength = 64;
	      break;
	  }

	  MessageDigest md;
	  int iterations = (int) Math.ceil(length / hashLength);
	  byte[] expansion = new byte[0];

	  byte[] A = data;

	  try {

	    md = MessageDigest.getInstance(digestAlgorithm);

	    for (int i = 0; i < iterations; i++) {
		    A = Handshaker.doHMAC(md, secret, A);
		    expansion = ByteArrayUtils.concatenate(expansion, Handshaker.doHMAC(md, secret, ByteArrayUtils.concatenate(A, data)));
	    }

	  }

	  catch (NoSuchAlgorithmException e) {
	    e.printStackTrace();
	  }

	  return ByteArrayUtils.truncate(expansion, length);
  }

  public static byte[] computeKey(byte[] macKey, byte[] data) {

      return PRF(macKey, data);

  }

  // The less significant part of KS value is stored in the bytes with highest index
  public static int computeKeyA(byte[] KS) {
    int key = 0;
    byte[] byteKey = ByteBuffer.allocate(4).putInt(key).array();
    byteKey[2] = KS[2];
    byteKey[3] = KS[3];
    key = (int)(ByteBuffer.wrap(byteKey).getInt());
    return key;
  }

  // The less significant part of KS value is stored in the bytes with highest index
  public static int computeKeyB(byte[] KS) {
    int key = 0;
    byte[] byteKey = ByteBuffer.allocate(4).putInt(key).array();
    byteKey[2] = KS[0];
    byteKey[3] = KS[1];
    key = (int)(ByteBuffer.wrap(byteKey).getInt());
    return key;
  }

  // The less significant part of KSj value is stored in the bytes with highest index
  public static int computeKeyCj(byte[] KSj, int index) {
    // debug //Rikard
    /*
    System.out.println("computeKeyCj(byte[] KSj, int index)");
    System.out.println("index: " + index);
    System.out.print("KSj: ");
    for (byte bi : KSj)
      System.out.format("0x%x ", bi);
    System.out.println("\n");
    */

    int key = 0;
    byte[] byteKey = ByteBuffer.allocate(4).putInt(key).array();
    int firstLogicByte = (((index + 2) % 16) * 16) / 8;
    int firstArrayByte = (30 - firstLogicByte) % 31;
    byteKey[2] = KSj[firstLogicByte];
    byteKey[3] = KSj[firstLogicByte+1];
    key = (int)(ByteBuffer.wrap(byteKey).getInt());
    return key;
  }

  public static byte[] computeSessionKeyFromID(byte[] masterSessionKey, int msgID) {
    byte[] byteMsgID = new byte[2];
    byte[] auxByteMsgID = ByteBuffer.allocate(4).putInt(msgID).array(); // the message ID is in the most significant bytes
    System.arraycopy(auxByteMsgID, 2, byteMsgID, 0, 2); // the most significant part of the message ID is the less significant byte, i.e. byteMsgID[0]
    return SMACKEngine.computeKey(masterSessionKey, byteMsgID);
  }

  public static byte[] computeShortMAC(Message msg, byte[] KS, byte[] KSj, int index) {
    int tokenSize = OptionNumberRegistry.TOKEN_LEN; // in bytes
    int numChunks = 2 + ((tokenSize - 2) / 2); // 2 bytes each

    int keyA = computeKeyA(KS);
    int keyB = computeKeyB(KS);
    int keyCj = computeKeyCj(KSj, index);
    //System.out.println("index: " + index);

    // debug
    /*
    System.out.println("KS"); // debug
    for (byte bi : KS)
	System.out.format("0x%x ", bi);
    System.out.println("\n"); // debug

    System.out.println("KSj"); // debug
    for (byte bi : KSj)
	System.out.format("0x%x ", bi);
    System.out.println("\n"); // debug

    System.out.format("KeyA %d\n", keyA); // debug
    System.out.format("KeyB %d\n", keyB); // debug
    System.out.format("KeyCj %d\n\n", keyCj); // debug

    System.out.format("Message index %d\n\n", index); // debug
    */

    //Changed to use DataSerializer //Rikard
    byte[] messageBytes = null;
    if(msg instanceof Request)
      messageBytes = serializer.serializeRequest((Request)msg);
    else if(msg instanceof Response)  //TODO: Maybe this can only be type Response? //Rikard
      messageBytes = serializer.serializeResponse((Response)msg);
    if(messageBytes == null)
      System.out.println("ERROR: messageBytes is NULL.");
    int[] chunks = new int[numChunks];

    // debug
    //System.out.print("computeShortMAC Message content: ");
    //for (byte bi : messageBytes)
    //  System.out.format("0x%x ", bi);
    //System.out.println("");

    for (int i = 0; i < numChunks; i++) {
      chunks[i] = 0;
      byte[] byteChunk = ByteBuffer.allocate(4).putInt(chunks[i]).array();
      byteChunk[2] = messageBytes[(i*2)]; // Most significant part of the chunk
      //System.out.println(chunks[i]); // debug
      byteChunk[3] = messageBytes[(i*2)+1]; // Less significant part of the chunk

      chunks[i] = (int)(ByteBuffer.wrap(byteChunk).getInt());
      //System.out.println(chunks[i]); // debug

      // debug

      //for (byte bi : byteChunk)
	  //  System.out.format("computeShortMAC Chunk: 0x%x\n", bi);
      //	System.out.println("");

      }

    int result = 0;
    result = GF.add(chunks[0], GF.multiply(keyA, chunks[1]));


    //System.out.format("result1 %d\n", result); // debug


    for (int i = 2; i < numChunks; i++) {
      int app = GF.multiply(GF.power(keyA, i), chunks[i]);
      result = GF.add(result, app);
    }

    //System.out.format("result2 %d\n", result);  // debug

    result = GF.multiply(result, keyB);
    //System.out.format("result3 %d\n", result); // debug

    //System.out.println("result: " + result);
    //System.out.println("keyCj: " + keyCj);
    result = GF.add(result, keyCj);
    //System.out.format("result4 %d\n", result); // debug

    byte[] byteResult = ByteBuffer.allocate(4).putInt(result).array();
    byte[] shortMAC = new byte[2];

    // The value is stored in the bytes with highest index
    System.arraycopy(byteResult, 2, shortMAC, 0, 2);

    // debug

    //for (byte bi : shortMAC)
      //System.out.format("0x%x ", bi);
    //System.out.println("\n");


    return shortMAC;

  }

  public static void addShortMAC(Message msg, byte[] shortMAC) {
    byte[] token = msg.getToken();

    /* DEBUG
    for (byte bi : token)
	System.out.format("0x%x ", bi);
	System.out.println("\n");
    */

    // The value must be stored in the two bytes with highest index
    System.arraycopy(shortMAC, 0, token, token.length - 2, 2);
    msg.setToken(token);
  }

  public static void clearEmbeddedRequestShortMAC(Message msg) {
    byte[] token = msg.getToken();
    token[token.length - 2] = token [token.length - 1] = 0;
    msg.setToken(token);
    /*if (msg instanceof Response) {
      ((Response)msg).getRequest().setToken(token); // this is necessary to correctly remove the ORIGINAL right token stored by the Token layer
    }*/ //FIXME: getRequest() can be done on an Exchange but not a Response //Rikard
  }

  public static boolean checkShortMAC(Message msg, byte[] KS, byte[] KSj, int index) {
    boolean ret = false;

    //Changed to use DataSerializer //Rikard
    byte[] messageBytes = null;
    if(msg instanceof Request)  //TODO: Maybe this can only be type Request? //Rikard
      messageBytes = serializer.serializeRequest((Request)msg);
    else if(msg instanceof Response)
      messageBytes = serializer.serializeResponse((Response)msg);
    if(messageBytes == null)
      System.out.println("ERROR: messageBytes is NULL.");

    byte[] retrievedShortMAC = new byte[2];
    int beginShortMAC = 4 + OptionNumberRegistry.TOKEN_LEN - 2;
    System.arraycopy(messageBytes, beginShortMAC, retrievedShortMAC, 0, 2);

    byte[] computedShortMAC = computeShortMAC(msg, KS, KSj, index);

    return Arrays.equals(retrievedShortMAC, computedShortMAC);
  }

}
