package org.eclipse.californium.SMACKEnvironment;

import java.util.Arrays;
import java.util.HashMap;
//import org.eclipse.californium.coap.EndpointAddress; //Disabled //Rikard
import java.lang.CloneNotSupportedException;
import org.eclipse.californium.coap.SMACKengine.SMACKEngine;
import org.eclipse.californium.core.network.config.NetworkConfig;

public class SMACKClientSession extends SMACKSession {

  private NetworkConfig config = NetworkConfig.getStandard();
  protected int initialMessageID;
  protected byte[] KSj = new byte[keySize]; // Current fragment session key used to derive c_i (initialized to KS)
  protected int messageCounter; // Counter of messages sent during this session
  protected int j; // Session portion identifier, used to compute the specific KSj to be used
  private int SMACKKeySize = config.getInt(NetworkConfig.Keys.SMACK_KEY_SIZE);
  private int messageIDIncrement = config.getInt(NetworkConfig.Keys.SMACK_P);

  public SMACKClientSession(int firstID, byte[] sessionKey) {
    super(firstID, sessionKey);
    initialMessageID = firstID;

    System.arraycopy(KS, 0, KSj, 0, KS.length);
    messageCounter = 0;
    j = 0;
  }

  public int getInitialMessageID() { return initialMessageID; }

  public int getMessageCounter() { return messageCounter; }

  public int getSessionPortion() { return j; }

  public byte[] getFragmentSessionKey() { return KSj; }

  public void incrementMessageCounter() { messageCounter++; }

  public void setSessionPortion(int portion) { j = portion; }

  public void updateFragmentSessionKey() {
    KSj = SMACKEngine.PRF(KS, KSj);
  }

}
