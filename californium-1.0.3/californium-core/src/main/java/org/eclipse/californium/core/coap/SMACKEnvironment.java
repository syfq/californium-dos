package org.eclipse.californium.SMACKEnvironment;

import org.eclipse.californium.core.network.config.NetworkConfig;

public class SMACKEnvironment {
  
  private static NetworkConfig config = NetworkConfig.getStandard();
  
  protected static int p = config.getInt(NetworkConfig.Keys.SMACK_SESSION_LENGTH); // session length (it must be a prime number)
  protected int keySize = config.getInt(NetworkConfig.Keys.SMACK_KEY_SIZE); // size of SMACK keys (in bytes)

}
