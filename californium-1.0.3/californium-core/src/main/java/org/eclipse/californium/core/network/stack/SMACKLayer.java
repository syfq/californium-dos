package org.eclipse.californium.core.network.stack;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.nio.ByteBuffer;
import java.util.logging.Logger; //Rikard
import java.util.Random; //Rikard
import org.eclipse.californium.SMACKEnvironment.SMACKClientSession;
import org.eclipse.californium.SMACKEnvironment.SMACKServerEnvironment;
import org.eclipse.californium.SMACKEnvironment.SMACKServerSession;
import org.eclipse.californium.coap.SMACKepoch.SMACKEpoch;
import org.eclipse.californium.scandium.util.ByteArrayUtils;
import org.eclipse.californium.core.network.config.NetworkConfig; //Rikard
import org.eclipse.californium.core.network.Exchange; //Rikard
import org.eclipse.californium.core.coap.EmptyMessage; //Rikard
import org.eclipse.californium.core.coap.Message;
import org.eclipse.californium.core.coap.Response;
import org.eclipse.californium.core.coap.Request;
import org.eclipse.californium.coap.SMACKengine.SMACKEngine;
import org.eclipse.californium.core.coap.OptionNumberRegistry;
import org.eclipse.californium.SMACKEnvironment.SMACKClientEnvironment;
import org.eclipse.californium.scandium.dtls.cipher.CCMBlockCipher;
import org.eclipse.californium.core.network.serialization.DataSerializer; //Rikard

//import org.apache.commons.lang.builder.ToStringBuilder; // debugging purpose //syafiq

import java.net.URI; // syafiq
import java.net.URISyntaxException; // syafiq
import org.eclipse.californium.core.coap.OptionSet; // syafiq

// State change
import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import java.net.InetSocketAddress;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.coap.CoAP.Code;
import java.text.SimpleDateFormat;
import java.util.Calendar;

// add reference to the secbox

public class SMACKLayer extends AbstractLayer {

  // exit codes for runtime errors //syafiq
	private static final int ERR_MISSING_METHOD  = 1;
	private static final int ERR_UNKNOWN_METHOD  = 2;
	private static final int ERR_MISSING_URI     = 3;
	private static final int ERR_BAD_URI         = 4;
	private static final int ERR_REQUEST_FAILED  = 5;
	private static final int ERR_RESPONSE_FAILED = 6;
	private static final int ERR_BAD_LINK_FORMAT = 7;

  private static NetworkConfig config = NetworkConfig.getStandard(); //Rikard

  private static boolean SMACKActive = config.getBoolean(NetworkConfig.Keys.SMACK_ACTIVE);
  private static boolean SMACKAppSecurity = config.getBoolean(NetworkConfig.Keys.SMACK_APP_SECURITY);
  private static int SMACKAuthFieldSize = config.getInt(NetworkConfig.Keys.SMACK_AUTH_FIELD_SIZE);
	private static boolean isServer = config.getBoolean(NetworkConfig.Keys.IS_SERVER);
  private static boolean isClient = config.getBoolean(NetworkConfig.Keys.IS_CLIENT);
  private static boolean isProxy  = config.getBoolean(NetworkConfig.Keys.IS_PROXY);
	private static int SMACKMaxMID = config.getInt(NetworkConfig.Keys.SMACK_MAX_MESSAGE_ID);

  byte[] pairwiseKey = new byte[] { 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }; // 16 bytes

  public static SMACKServerEnvironment environment;

  private SMACKEpoch epoch;

  //private static SMACKClientEnvironment clientEnvironment; // SMACK Client environment

  private DataSerializer serializer = new DataSerializer(); //For getting bytes of a message //Rikard

  private Random random = new Random(); //For generation of Tokens //Rikard
	URI uriSendReq = null;
	URI uriRcvReq = null;

	private byte[] masterKey = null;
	private byte[] seed = null;
	public static boolean useSMACK;

	/**
	change state from normal to protected
	ST 0 : server to client
	ST 1 : server to KDC
	ST 2 : ack KDC to Server

	change state from protected to off
	ST 3 : server to KDC
	ST 4 : ack KDC to server

	change state from off to protected
	ST 5 : server to KDC
	ST 6 : ack KDC to server
	ST 7 : KDC to client

	change state from protected to normal
	ST 8 : server to KDC
	ST 9 : ack KDC to server
	ST 10: KDC to all client
	**/

	// State
	private static byte[] State_NP_ServerToClient = new byte[] { 0x01, 0x00, };
	private static byte[] State_NP_ServerToKDC 	 	= new byte[] { 0x01, 0x01, };
	private static byte[] State_NP_KDCToServer 	 	= new byte[] { 0x01, 0x02, };
	private static byte[] State_PO_ServerToKDC 	 	= new byte[] { 0x01, 0x03, };
	private static byte[] State_PO_KDCToServer 	 	= new byte[] { 0x01, 0x04, };
	private static byte[] State_PO_KDCToClient	 	= new byte[] { 0x01, 0x05, };
	private static byte[] State_OP_ServerToKDC 	 	= new byte[] { 0x01, 0x06, };
	private static byte[] State_OP_KDCToServer 	 	= new byte[] { 0x01, 0x07, };
	private static byte[] State_OP_KDCToClient 	 	= new byte[] { 0x01, 0x08, };
	private static byte[] State_PN_ServerToKDC 	 	= new byte[] { 0x01, 0x09, };
	private static byte[] State_PN_KDCToServer 	 	= new byte[] { 0x01, 0x0a, };
	private static byte[] State_PN_KDCToClient 	 	= new byte[] { 0x01, 0x0b, };

	private static String identity = "Client_identity";
  private static String psk = "secretPSK";

  /* Below are the new methods for passing messages between layers //Rikard */
  /* =========================================================== */

    //Method for sending Request messages. These should be protected with a MAC. //Rikard
    //@Override

	public void sendRequest(Exchange exchange, Request request) {
    //System.out.println("SMACKLayer: sendRequest");
    String strSendReq = request.getURI();

    try {
			uriSendReq = new URI(strSendReq);
		} catch (URISyntaxException e) {
			System.err.println("Failed to parse URI: " + e.getMessage());
			System.exit(ERR_BAD_URI);
		}
    Integer portSendReq = uriSendReq.getPort();
    Integer kdcSendReq = 5684;
    // future dev = could be specified by IP, Port, or anything else. depends on the proxy/KDC condition. //syafiq
    if (portSendReq.equals(kdcSendReq)) {
			LOGGER.info(String.format("SMACKLayer: Sending Request WITHOUT SMACK Layer"));
      super.sendRequest(exchange, request);
    } else {
			LOGGER.info(String.format("SMACKLayer: Sending Request WITH SMACK Layer"));
    	Message msg = request;
    	SMACKClientSession s = request.getClientSession(); //Gets relevant SMACK client session from Request
    	long startLayerCrossing = System.nanoTime();

    	//Generate random Token if it is not already set //Rikard
    	if(request.getToken() == null) {
      	byte[] token = new byte[OptionNumberRegistry.TOKEN_LEN];
      	random.nextBytes(token);
      	request.setToken(token);
    	}
    	// remember when this message was sent for the first time
    	// set timestamp only once in order
    	// to handle retransmissions correctly
    	if (msg.getTimestamp() == -1) {
	    	msg.setTimestamp(startLayerCrossing);
	    	// LOGGER.info(String.format("doSendMessageSMACK timestamp: %d", msg.getTimestamp())); // debug M.T.
    	}

    	/*if(!(msg instanceof Request)) { //TODO: Move to sendResponse? (although this already seems to exist there) //Rikard

      if (SMACKAppSecurity) {
				long timeBeforeProtection = System.nanoTime(); // DEBUG
				securePayload(msg);
				System.out.println("Payload protection: " + String.valueOf(System.nanoTime() - timeBeforeProtection) + " ns"); // DEBUG
      }

      // M.T. debug
      long timestamp = System.nanoTime();
      long crossingTime = timestamp - startLayerCrossing;
      System.out.println("SMACK outgoing crossing " + String.valueOf(crossingTime) + " ns");
      // end M.T.

      //sendMessageOverLowerLayer(msg);
      super.sendRequest(exchange, response); //Rikard
      return;

    	}*/

    	// Else, a Request message must be sent

    	//Code for handling the message ID of outgoing message and updating client session parameters //Rikard
    	//This code was previously in the TransactionLayer for the old implementation. That layer no longer exists.
    	int ID = s.getLastID();
    	s.incrementLastID();
    	msg.setMID(ID);
    	s.incrementMessageCounter();

    	// The message counter in the session structure has been updated by the Transaction layer, as this message would have been already sent.
    	// Hence, for the i-th message (i = 0, 1, ...), the portion index j = floor((i+2)/16) is computed as follows
    	int portion = (s.getMessageCounter() + 1) / 16;

    	if(portion != s.getSessionPortion()) {
      	s.setSessionPortion(portion);
      	s.updateFragmentSessionKey();
    	}

    	// The message counter in the session structure has been updated by the Transaction layer, as this message would have been already sent
    	//long timeBeforeShortMAC = System.nanoTime(); // DEBUG
    	byte[] shortMAC = SMACKEngine.computeShortMAC(msg, s.getSessionKey(), s.getFragmentSessionKey(), s.getMessageCounter() - 1);
    	//String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
		//System.out.println(""+timeStamp+" Short MAC computation: " + String.valueOf(System.nanoTime() - timeBeforeShortMAC) + " ns"); // DEBUG
    	SMACKEngine.addShortMAC(msg, shortMAC);

    	LOGGER.info(String.format("Computed short MAC: %s", ByteArrayUtils.toHexString(shortMAC))); // debug

    	if (SMACKAppSecurity) {
      	long timeBeforeProtection = System.nanoTime(); // DEBUG
      	securePayload(msg);
      	System.out.println("Payload protection: " + String.valueOf(System.nanoTime() - timeBeforeProtection) + " ns"); // DEBUG
    	}

    	// M.T. debug
    	long timestamp = System.nanoTime();
    	//long crossingTime = timestamp - startLayerCrossing;
    	//System.out.println(""+timeStamp+" SMACK outgoing crossing " + String.valueOf(crossingTime) + " ns");
    	// end M.T.

    	// Send the message including the computed short MAC
    	//sendMessageOverLowerLayer(msg);

    	System.out.println("MessageID in SMACKLayer: sendRequest: " + request.getMID());
    	super.sendRequest(exchange, request); //Rikard
  	}
  }

  // Invoked to send response messages, which do never include a short MAC, but have to be protected in case SMACK security services are activate //M.T.
  // Changed the parameters to take Exchange & Request instead of Message. //Rikard
  @Override
  public void sendResponse(Exchange exchange, Response response) { //CHECKED
    //System.out.println("SMACKLayer: sendResponse");
    Message msg = response; //Get message here instead of in parameter //Rikard
    long startLayerCrossing = System.nanoTime();
    if (SMACKAppSecurity) {
    	// remember when this message was sent for the first time
      // set timestamp only once in order
      // to handle retransmissions correctly
      if (msg.getTimestamp() == -1) {
				msg.setTimestamp(startLayerCrossing);
				// LOGGER.info(String.format("doSendMessage timestamp: %d", msg.getTimestamp())); // debug M.T.
      }

      long timeBeforeProtection = System.nanoTime(); // DEBUG
				securePayload(msg);
					System.out.println("Payload protection: " + String.valueOf(System.nanoTime() - timeBeforeProtection) + " ns"); // DEBUG
    }

		// Dos-Robustness Signalling
		// send notification to client to send the request to proxy instead
		// syafiq
		if (!SecurityBox.getIsServerNormal() && SecurityBox.getIsServerProtected() && !SecurityBox.getIsServerOff() && isServer) {
			System.out.println("Send notification to client, this server is going to protected mode");
			msg.setPayload(State_NP_ServerToClient);
		//	System.out.println("msg ="+ToStringBuilder.reflectionToString(msg.getPayload()));
		}

    // M.T. debug
    //long timestamp = System.nanoTime();
    //long crossingTime = timestamp - startLayerCrossing;
    //String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
	//System.out.println(""+timeStamp+" SMACK outgoing crossing " + String.valueOf(crossingTime) + " ns");
    // end M.T.

    // delegate to the lower layer
    //sendMessageOverLowerLayer(msg);
    super.sendResponse(exchange, response);
  }

  /* Receives an incoming request, here the MAC should be verified //Rikard */
  @Override
  public void receiveRequest(Exchange exchange, Request request) { //CHECKED

    //System.out.println("SMACKLayer: receiveRequest");
    String strRcvReq = request.getURI();
    try {
			uriRcvReq = new URI(strRcvReq);
		} catch (URISyntaxException e) {
			System.err.println("Failed to parse URI: " + e.getMessage());
			System.exit(ERR_BAD_URI);
		}
    Integer portRcvReq = uriRcvReq.getPort();
    Integer kdcRcvReq = 5684;
    if (portRcvReq.equals(kdcRcvReq)) { // SMACK Only start if it receive SMACK material.
			//LOGGER.info(String.format("SMACKLayer: Receiving Request WITHOUT SMACK Layer"));
      super.receiveRequest(exchange, request);
    } else {
			//LOGGER.info(String.format("SMACKLayer: Receiving Request WITH SMACK Layer"));

			if (!SecurityBox.getIsServerNormal() && !SecurityBox.getIsServerProtected() && SecurityBox.getIsServerOff() && isServer) {
				System.out.println("This server is in OFF state, disregards any kind of incoming coap request");
				request.cancel();
			//	System.out.println("msg ="+ToStringBuilder.reflectionToString(msg.getPayload()));
			} else {
    		Message msg = request; //Get message here //Rikard

    		long startLayerCrossing = System.nanoTime();

    		// debug
    		//byte[] messageBytes = msg.getBytes(); //Rikard
    		byte[] messageBytes = serializer.serializeRequest(request); //Rikard
    		//System.out.println("computeShortMAC Message content");
    		//for (byte bi : messageBytes)
      		//System.out.format("0x%x ", bi);
    		//System.out.println(""); // debug

    		// Else, a Request message has been received
    		//LOGGER.info(String.format("Incoming request: %s", msg.sequenceKey())); // return source and token

    		if(epoch.getAcceptFutureSessions() && !epoch.getActive()) { // epoch startup
      		//System.out.println("epochStartup(exchange, request, startLayerCrossing);");
      		epochStartup(exchange, request, startLayerCrossing);

    		} else if(epoch.getActive()) { // ongoing epoch
      		//System.out.println("manageOngoingEpoch(exchange, request, startLayerCrossing);");
      		manageOngoingEpoch(exchange, request, startLayerCrossing);
					// syafiq
					if (msg.getMID() == SMACKMaxMID) {
						// report to KDC to start over
						SecurityBox.runServerInitCont();
						System.out.println("Start over everything");
						epoch.setActive(false);
					}
    		} else {
      		// This should never happen
      		LOGGER.info(String.format("SMACK epoch is in an inconsistent state!\n"));
    		}
			}
  	}
  }

  //Method for receiving responses to requests. MAC is not checked but token modified. //Rikard
  @Override
	public void receiveResponse(Exchange exchange, Response response) {
    System.out.println("SMACKLayer: receiveResponse");
    Message msg = response; //Get message here //Rikard

    long startLayerCrossing = System.nanoTime();

    if(!(msg instanceof Request)) {
      // Set the last two bytes of the token field to zero, before delivery to the layers above
      if (SMACKActive) {

				long timeBeforeUnprotection = System.nanoTime(); // DEBUG

				// Unsecure the payload in case application security is active
				if (SMACKAppSecurity && !unsecurePayload(msg))
	  			return;

				if (SMACKAppSecurity)
	  			System.out.println("Payload unprotection: " + String.valueOf(System.nanoTime() - timeBeforeUnprotection) + " ns"); // DEBUG

				byte[] newToken = msg.getToken();
				newToken[newToken.length - 2] = newToken[newToken.length - 1] = 0;
				msg.setToken(newToken);
      }

      long timestamp = System.nanoTime();
      msg.setTimestamp(timestamp);

      // M.T. debug
      long crossingTime = timestamp - startLayerCrossing;
      String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
	  System.out.println(""+timeStamp+" SMACK incoming crossing " + String.valueOf(crossingTime) + " ns");
      // end M.T.
      super.receiveResponse(exchange, response);
			return;
    }
  }


  /* =========================================================== */

  /* Below is the original code from the Californium 0.13.7 implementation of SMACK with modifications //Rikard */

  //Method for starting an epoch. Changed the parameters to take Exchange & Request instead of Message. //Rikard

	//public static void getSMACKServerEnvironment() {
	//	return environment;
	//}

	// syafiq
	public static void setSMACKServerEnvironment(byte[] masterKey, byte[] seed) {
		environment = new SMACKServerEnvironment(masterKey, seed);
	}

	private void epochStartup(Exchange exchange, Request request, long startLayerCrossing) {
    Message msg = request; //Get message here instead of in parameter //Rikard
    int msgID = msg.getMID();
    int firstID = epoch.getFirstID();
    boolean found = false;
    int windowSize = epoch.getAcceptanceWindowSize();
    int sessionSize = config.getInt(NetworkConfig.Keys.SMACK_P);
    int auxID;

	//System.out.println("msgID " + msgID);
    //System.out.println("firstID " + firstID);
    //System.out.println("windowSize " + windowSize);
    //System.out.println("sessionSize " + sessionSize);

    for(int i = 0; i < windowSize; i++) {
      auxID = (firstID + i * sessionSize) % 65536;
      if(msgID == auxID) {
				found = true;
				break;
      }
    }

    // This message is not valid as a first message to start an epoch, and it is silently discarded
    if(found == false) {
      System.out.println("Invalid first message");
			// Increase Invalid Counter
			SecurityBox.InvalidCounterIncrement(); //syafiq
			System.out.println("SecurityBox.getCounterValue ="+SecurityBox.getCounterValue());

			// Dos-Robustness Signalling
			// send notification to KDC to act as a relay from client
			// syafiq
			//if ((SecurityBox.getCounterValue() == 9) && isServer) {
			//if (!SecurityBox.getIsServerNormal() && SecurityBox.getIsServerProtected() && !SecurityBox.getIsServerOff() && isServer) {
			//	LOGGER.info(String.format("SERVER : Send notification to KDC to act as a relay"));
			//	SecurityBox.NPNotifKDC();
			//}
      return;
    }

    // Else, check the short MAC conveyed by the message

    byte[] token = msg.getToken();
    byte[] receivedShortMAC = new byte[2];

    // DEBUG
    //for (byte bi : token)
    //  System.out.format("0x%x ", bi);
    //System.out.println("\n");

    //System.out.format("0x%x ", token[OptionNumberRegistry.TOKEN_LEN - 2]);
    //System.out.format("0x%x ", token[OptionNumberRegistry.TOKEN_LEN - 1]);
    //System.out.println("\n");

    System.arraycopy(token, OptionNumberRegistry.TOKEN_LEN - 2, receivedShortMAC, 0, 2);
		//byte[] testtest = environment.getMasterSessionKey();
		//System.out.println("KMS ="+ToStringBuilder.reflectionToString(testtest));

		//System.out.println(environment);
		byte[] sessionKey = SMACKEngine.computeSessionKeyFromID(environment.getMasterSessionKey(), msgID);

    long timeBeforeShortMAC = System.nanoTime(); // DEBUG
    byte[] computedShortMAC = SMACKEngine.computeShortMAC(msg, sessionKey, sessionKey, 0); // It is the first session message, hence KSj == KS, and index i = 0
		//LOGGER.info(String.format("receivedShortMAC %s", ByteArrayUtils.toHexString(receivedShortMAC))); // debug
		//LOGGER.info(String.format("computedShortMAC %s", ByteArrayUtils.toHexString(computedShortMAC))); // debug

	//String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
    //System.out.println(""+timeStamp+" Short MAC computation: " + String.valueOf(System.nanoTime() - timeBeforeShortMAC) + " ns"); // DEBUG

    // This message is not authentic and is silently discarded
    if(!Arrays.equals(receivedShortMAC, computedShortMAC)) {
      SecurityBox.InvalidCounterIncrement(); // syafiq
	  //LOGGER.info(String.format("Fist epoch message - Invalid short MAC %s", ByteArrayUtils.toHexString(receivedShortMAC))); // debug
      return;
    }

    // Else, the message is authentic, and valid to start the new epoch
    //LOGGER.info(String.format("Fist epoch message - Valid short MAC %s", ByteArrayUtils.toHexString(receivedShortMAC))); // debug

    long timeBeforeUnprotection = System.nanoTime(); // DEBUG

    // Unsecure the payload in case application security is active
    if (SMACKAppSecurity && !unsecurePayload(msg))
			return;

    if (SMACKAppSecurity)
      System.out.println("Payload unprotection: " + String.valueOf(System.nanoTime() - timeBeforeUnprotection) + " ns"); // DEBUG

    epoch.startNewEpoch(msgID);
    environment.addSession(msgID, sessionKey);

    /* In case SMACK is active, update the timestamp of the received message, replacing the value written by the UDP layer below */
    long timestamp = System.nanoTime();
    msg.setTimestamp(timestamp);

    // M.T. debug
    long crossingTime = timestamp - startLayerCrossing;
    //String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
	//System.out.println(""+timeStamp+" SMACK incoming crossing " + String.valueOf(crossingTime) + " ns");
    // end M.T.

    super.receiveRequest(exchange, request); //Changed //Rikard
  }


  // TODO Extend this function in order to address the end of a SMACK epoch and the creation of a new one //M.T.
  //Method for continuing an epoch. Changed the parameters to take Exchange & Request instead of Message. //Rikard
  private void manageOngoingEpoch(Exchange exchange, Request request, long startLayerCrossing) {

    //super.receiveRequest(exchange, request); //Changed (Rikard) // decomment to test SMACK without processing short MAC after the first message (comment everything below) //M.T.

    Message msg = request; //Rikard
    int msgID = msg.getMID();
    int windowSize = epoch.getAcceptanceWindowSize();
    int sessionSize = config.getInt(NetworkConfig.Keys.SMACK_P);
    int windowsUpdateStep = config.getInt(NetworkConfig.Keys.SMACK_WINDOW_UPDATE_STEP);

    //System.out.println("msgID " + msgID);
    //System.out.println("windowSize " + windowSize);
    //System.out.println("sessionSize " + sessionSize);
    //System.out.println("windowsUpdateStep " + windowsUpdateStep);

    int compID = 0;
    boolean newSessionMessage =  false;

    if (epoch.getAcceptFutureSessions() == true) {

      // Check if it is the first message of a new session
      for (int i = 1; i < windowSize; i++) {
				compID = (epoch.getOldestSessionInitialID() + (i * sessionSize)) % 65536;
				// debug syafiq
				//System.out.println("check2=========");
				//System.out.println("msgID  ="+msgID);
				//System.out.println("compID ="+compID);
				//System.out.println("check2=========");
				// Check also that this message is not a reply
				if (msgID == compID) {
	  			if (epoch.isSessionActive(msgID) == false) {
	    			newSessionMessage = true;
	    			break;
	  			// Replay attack with an old first message in an active session
					} else {
						SecurityBox.InvalidCounterIncrement(); // syafiq
	    			LOGGER.info(String.format("Invalid message: replay of an old first message in an already active session!"));
	    			SecurityBox.InvalidCounterIncrement();
					return;
	  			}
				}
      }


      // It is the first message of a new session
      if (newSessionMessage) {

				// Check the message short MAC
				byte[] token = msg.getToken();
				byte[] receivedShortMAC = new byte[2];

				// DEBUG
				//for (byte bi : token)
	  			//System.out.format("0x%x ", bi);
				//System.out.println("\n");

				//System.out.format("0x%x ", token[OptionNumberRegistry.TOKEN_LEN - 2]);
				//System.out.format("0x%x ", token[OptionNumberRegistry.TOKEN_LEN - 1]);
				//System.out.println("\n");


				System.arraycopy(token, OptionNumberRegistry.TOKEN_LEN - 2, receivedShortMAC, 0, 2);
				byte[] sessionKey = SMACKEngine.computeSessionKeyFromID(environment.getMasterSessionKey(), msgID);

				//long timeBeforeShortMAC = System.nanoTime(); // DEBUG
				byte[] computedShortMAC = SMACKEngine.computeShortMAC(msg, sessionKey, sessionKey, 0); // It is the first session message, hence KSj == KS, and index i = 0
				//String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
				//System.out.println(""+timeStamp+" Short MAC computation: " + String.valueOf(System.nanoTime() - timeBeforeShortMAC) + " ns"); // DEBUG

				// This message is not authentic and is silently discarded
				if(!Arrays.equals(receivedShortMAC, computedShortMAC)) {
	  			//LOGGER.info(String.format("Message in new session - Invalid short MAC %s", ByteArrayUtils.toHexString(receivedShortMAC))); // debug
	  			SecurityBox.InvalidCounterIncrement(); // syafiq
				return;
				}

				// Else, the message is authentic, and valid to start the new epoch
				//LOGGER.info(String.format("Message in new session - Valid short MAC %s", ByteArrayUtils.toHexString(receivedShortMAC))); // debug

				long timeBeforeUnprotection = System.nanoTime(); // DEBUG

				// Unsecure the payload in case application security is active
				if (SMACKAppSecurity && !unsecurePayload(msg))
	  			return;

				if (SMACKAppSecurity)
	  			System.out.println("Payload unprotection: " + String.valueOf(System.nanoTime() - timeBeforeUnprotection) + " ns"); // DEBUG

				environment.addSession(msgID, sessionKey);
				epoch.addActiveSessionInitialIDs(msgID);

				// Check if some old sessions must be removed as stale ones, according to the sliding window mechanism
				if (environment.getNumActiveSessions() == windowSize) {
	  			int oldestSessionID = epoch.getOldestSessionInitialID();

	  			for (int i = 0; i < windowsUpdateStep; i++) {
	    			epoch.removeActiveSessionInitialIDs(oldestSessionID); // remove the ID from the set of initial message ID of active sessions
	    			environment.removeSession(oldestSessionID); // remove the oldest active sessions
	    			oldestSessionID = (oldestSessionID + sessionSize) % 65536;
	  			}

	  			// SMACK_WINDOW_UPDATE_STEP sessions have been removed, and oldestSessionID contains the initial message ID of the currently oldest session
	  			epoch.setOldestSessionInitialID(oldestSessionID);
				}

				// Check if this is the "most recent" session that can be created in this epoch. If this is the case, disable the epoch to accept other "future" sessions
				if (msgID == epoch.getFinalID())
	  			epoch.setAcceptFutureSessions(false);

				/* In case SMACK is active, update the timestamp of the received message, replacing the value written by the UDP layer below */
				long timestamp = System.nanoTime();
				msg.setTimestamp(timestamp);

				// M.T. debug
				long crossingTime = timestamp - startLayerCrossing;
				//String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
				//System.out.println(""+timeStamp+" SMACK incoming crossing " + String.valueOf(crossingTime) + " ns");
				// end M.T.

				//deliverMessage(msg);
        super.receiveRequest(exchange, request); //Changed //Rikard
				return;

      } // end of new session message handling

    } // end of checking for new session first message

    // If it is not a message for a new session or new sessions are not allowed anymore, check if it is a message for an already existing session

    if (newSessionMessage == false) {

      int leftBoundary = epoch.getOldestSessionInitialID();
      int rightBoundary = (leftBoundary + sessionSize) % 65536;
      boolean foundActiveSession = false;

      for (int i = 0; i < windowSize; i++) {

				// Message ID 0 is NOT between leftBoundary and rightBoundary
				if (rightBoundary - leftBoundary == sessionSize) {
	  			if (leftBoundary < msgID && msgID < rightBoundary) {
	    			foundActiveSession = true;
	    			break;
	  			}
				} else { // leftBoundary is close to 2^16 - 1 and rightBoundary is close to 0, then (rightBoundary - leftBoundary) > p (since p << 2^16)
	  			if ( (leftBoundary < msgID && msgID <= 65535) || (0 <= msgID && msgID < rightBoundary)) {
	    			foundActiveSession = true;
	    			break;
	  			}
				}

				leftBoundary = rightBoundary;
				rightBoundary = (leftBoundary + sessionSize) % 65536;
      }

			//System.out.println("foundActiveSession ="+foundActiveSession);
      // The message belongs to an active session
      if (foundActiveSession) {
			// Check the message short MAC
			byte[] token = msg.getToken();
			byte[] receivedShortMAC = new byte[2];

			//for (byte bi : token)
	  		//System.out.format("0x%x ", bi);
			//System.out.println("\n");

			//System.out.format("0x%x ", token[OptionNumberRegistry.TOKEN_LEN - 2]);
			//System.out.format("0x%x ", token[OptionNumberRegistry.TOKEN_LEN - 1]);
			//System.out.println("\n");

			System.arraycopy(token, OptionNumberRegistry.TOKEN_LEN - 2, receivedShortMAC, 0, 2);

			SMACKServerSession s = environment.getSession(leftBoundary);

			if(s!=null) { // syafiq
			// Check if this a message already received within this session (replay attack)
			if (s.isOldMessage(msgID)) {
				SecurityBox.InvalidCounterIncrement(); // syafiq
	  		//LOGGER.info(String.format("Invalid message: replay of an old message in an already active session!"));
	  		return;
			}

			byte[] sessionKey = s.getSessionKey();

			int messageIndex = 0;

			// Determine the message position in the current session

			// Message ID 0 is NOT between leftBoundary and rightBoundary
			if (rightBoundary - leftBoundary == sessionSize) {
	  		messageIndex = msgID - leftBoundary;
			} else { // leftBoundary is close to 2^16 - 1 and rightBoundary is close to 0, then (rightBoundary - leftBoundary) > p (since p << 2^16)
	  		if (leftBoundary < msgID && msgID <= 65535) {
	    		messageIndex = msgID - leftBoundary;
	  		} else {// 0 <= msgID < rightBoundary
	    		messageIndex = (65536 - leftBoundary) + msgID;
				}
			}

			byte[] keyJ = null;
			int keyIndex = (messageIndex + 2) /16;

			// Retrieve or create the right Kj to be used to compute short MAC
			if (s.usedKey(keyIndex)) {
	  		keyJ = s.getKey(keyIndex);
			} else {
	  		keyJ = s.addKey(keyIndex);
			}

			//long timeBeforeShortMAC = System.nanoTime(); // DEBUG
			byte[] computedShortMAC = SMACKEngine.computeShortMAC(msg, sessionKey, keyJ, messageIndex);
			//String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
			//System.out.println(""+timeStamp+" Short MAC computation: " + String.valueOf(System.nanoTime() - timeBeforeShortMAC) + " ns"); // DEBUG

			// This message is not authentic and is silently discarded
			if(!Arrays.equals(receivedShortMAC, computedShortMAC)) {
				SecurityBox.InvalidCounterIncrement(); // syafiq
	  		//LOGGER.info(String.format("Message in existing session - Invalid short MAC %s", ByteArrayUtils.toHexString(receivedShortMAC))); // debug
	  		return;
			}

			// Else, the message is authentic, and valid to start the new epoch
			//LOGGER.info(String.format("Message in existing session - Valid short MAC %s", ByteArrayUtils.toHexString(receivedShortMAC))); // debug

			long timeBeforeUnprotection = System.nanoTime(); // DEBUG

			// Unsecure the payload in case application security is active
			if (SMACKAppSecurity && !unsecurePayload(msg))
	  		return;

			if (SMACKAppSecurity)
	  		System.out.println("Payload unprotection: " + String.valueOf(System.nanoTime() - timeBeforeUnprotection) + " ns"); // DEBUG

			// Add the message ID of the just received and processed message to the set of received message IDs in this session
			s.registerMessageID(msgID);

			/* In case SMACK is active, update the timestamp of the received message, replacing the value written by the UDP layer below */
			long timestamp = System.nanoTime();
			msg.setTimestamp(timestamp);

			// M.T. debug
			long crossingTime = timestamp - startLayerCrossing;
			//String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
			//System.out.println(""+timeStamp+" SMACK incoming crossing " + String.valueOf(crossingTime) + " ns");
			// end M.T.

      super.receiveRequest(exchange, request); //Changed //Rikard
			//deliverMessage(msg);
			return;
			}
    	}

  	}
    // This should never happen
    //LOGGER.info(String.format("Invalid message"));
		SecurityBox.InvalidCounterIncrement(); // syafiq
		//System.out.println("invalid messages ="+SecurityBox.getCounterValue());
  }

  //public void connectToSMACKClientEnvironment(SMACKClientEnvironment environment) {
  //  clientEnvironment = environment;
  //}

  public SMACKLayer(int A, int W, int ID) {
    /* TODO The Master Key and the seed value should be retrieved by an external configuration file, and are relevant only for server nodes */
    //byte[] masterKey = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, }; // 32 bytes
    //byte[] seed = new byte[] { 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, }; // 32 bytes

    //environment = new SMACKServerEnvironment(masterKey, seed);
		//environment = new SMACKServerEnvironment();
		//SMACKServerEnvironment environment;
		//secbox = new SecurityBox();
		//System.out.println(secbox);
		epoch = new SMACKEpoch(A, W, ID);
  }

  /*protected void doSendMessageSMACK(Message msg, SMACKClientSession s) throws IOException { //TODO: Delete? //Rikard

    long startLayerCrossing = System.nanoTime();

    // remember when this message was sent for the first time
    // set timestamp only once in order
    // to handle retransmissions correctly
    if (msg.getTimestamp() == -1) {
	    msg.setTimestamp(startLayerCrossing);
	    // LOGGER.info(String.format("doSendMessageSMACK timestamp: %d", msg.getTimestamp())); // debug M.T.
    }

    if(!(msg instanceof Request)) {

      if (SMACKAppSecurity) {
	long timeBeforeProtection = System.nanoTime(); // DEBUG
	securePayload(msg);
	System.out.println("Payload protection: " + String.valueOf(System.nanoTime() - timeBeforeProtection) + " ns"); // DEBUG
      }

      // M.T. debug
      long timestamp = System.nanoTime();
      long crossingTime = timestamp - startLayerCrossing;
      System.out.println("SMACK outgoing crossing " + String.valueOf(crossingTime) + " ns");
      // end M.T.

      //TODO: Re-do //Rikard  sendMessageOverLowerLayer(msg);
      return;

    }

    // Else, a Request message must be sent

    // The message counter in the session structure has been updated by the Transaction layer, as this message would have been already sent.
    // Hence, for the i-th message (i = 0, 1, ...), the portion index j = floor((i+2)/16) is computed as follows
    int portion = (s.getMessageCounter() + 1) / 16;

    if(portion != s.getSessionPortion()) {
      s.setSessionPortion(portion);
      s.updateFragmentSessionKey();
    }

    // The message counter in the session structure has been updated by the Transaction layer, as this message would have been already sent
    long timeBeforeShortMAC = System.nanoTime(); // DEBUG
    byte[] shortMAC = SMACKEngine.computeShortMAC(msg, s.getSessionKey(), s.getFragmentSessionKey(), s.getMessageCounter() - 1);
    System.out.println("Short MAC computation: " + String.valueOf(System.nanoTime() - timeBeforeShortMAC) + " ns"); // DEBUG
    SMACKEngine.addShortMAC(msg, shortMAC);

    LOGGER.info(String.format("Computed short MAC: %s", ByteArrayUtils.toHexString(shortMAC))); // debug

    if (SMACKAppSecurity) {
      long timeBeforeProtection = System.nanoTime(); // DEBUG
      securePayload(msg);
      System.out.println("Payload protection: " + String.valueOf(System.nanoTime() - timeBeforeProtection) + " ns"); // DEBUG
    }

    // M.T. debug
    long timestamp = System.nanoTime();
    long crossingTime = timestamp - startLayerCrossing;
    System.out.println("SMACK outgoing crossing " + String.valueOf(crossingTime) + " ns");
    // end M.T.

    // Send the message including the computed short MAC
    //TODO: Re-do //Rikard  sendMessageOverLowerLayer(msg);

  }*/


  /* Below are methods related to payload security //Rikard */ //TODO: Check if needed //Rikard
  /* =========================================================== */

  /* Secure the message payload (both encrypted and authenticated) */
  private void securePayload(Message msg) {
    // Create the 12 byte nonce by repeating the current Message ID six times
    byte[] nonce = new byte[12];
    byte[] noncePiece = ByteBuffer.allocate(4).putInt(0).array();

    int messageID = msg.getMID();
    byte[] byteMessageID = ByteBuffer.allocate(4).putInt(messageID).array();
    noncePiece[2] = byteMessageID[2];
    noncePiece[3] = byteMessageID[3];

    for (int i = 0; i < 6; i++)
      System.arraycopy(noncePiece, 2, nonce, i*2, 2);

    /*
    // debug
    System.out.println("payload");
    if (msg.getPayload() != null) {
      for (byte bi : msg.getPayload())
	System.out.format("0x%x ", bi);
      System.out.println(""); // debug
    }
    */

    if (msg.getPayload() == null) {
      //LOGGER.info(String.format("Payload is void\n")); // debug
      byte[] emptyPayload = new byte[0];
      msg.setPayload(emptyPayload);
    }

    // Prepare additional data to be authenticated but not encrypted (i.e. the CoAP header)
    byte[] byteMsg = msg.getBytes(); //Rikard
    if(byteMsg == null)
      System.out.println("ERROR: messageBytes is NULL.");
    int headerSize = byteMsg.length - msg.getPayloadSize();
    if (msg.getPayloadSize() != 0) // In case of non empty payload, do not take into account the PAYLOAD_MARKER byte 0xFF
      headerSize --;
    byte[] additionalData = new byte[headerSize];
    System.arraycopy(byteMsg, 0, additionalData, 0, headerSize);

    /*
    // debug
    System.out.println("additional data");
    for (byte bi : additionalData)
      System.out.format("0x%x ", bi);
    System.out.println(""); // debug
    */

    byte[] encryptedPayload = new byte[] { 0 };
    try {
      encryptedPayload = CCMBlockCipher.encrypt(pairwiseKey, nonce, additionalData, msg.getPayload(), SMACKAuthFieldSize);
    } catch (Exception e) {
      LOGGER.warning("ERROR: CCM Payload encryption failed. " + e.getMessage());
    }

    LOGGER.info(String.format("Message authenticated, length: %d", encryptedPayload.length)); // debug

    // Replace the plain payload with the secured one
    msg.setPayload(encryptedPayload);
  }

  /* Unsecure the message payload (both encrypted and authenticated) */
  private boolean unsecurePayload(Message msg) {
    // Create the 12 byte nonce by repeating the current Message ID six times
    byte[] nonce = new byte[12];
    byte[] noncePiece = ByteBuffer.allocate(4).putInt(0).array();

    int messageID = msg.getMID();
    byte[] byteMessageID = ByteBuffer.allocate(4).putInt(messageID).array();
    noncePiece[2] = byteMessageID[2];
    noncePiece[3] = byteMessageID[3];

    for (int i = 0; i < 6; i++)
      System.arraycopy(noncePiece, 2, nonce, i*2, 2);

    /*
    // Additional data is empty (the payload is the only part of the message to be secured)
    byte[] additionalData = new byte[0];
    */

    // Prepare additional data to be verified but not decrypted (i.e. the CoAP header)
    byte[] byteMsg = msg.getBytes(); //Rikard
    if(byteMsg == null)
      System.out.println("ERROR: messageBytes is NULL.");
    int headerSize = byteMsg.length - msg.getPayloadSize() - 1; // Do not take into account the PAYLOAD_MARKER byte 0xFF (A message with a secure empty payload cannot be received, because of the MIC presence)
    byte[] additionalData = new byte[headerSize];
    System.arraycopy(byteMsg, 0, additionalData, 0, headerSize);

    /*
    // debug
    System.out.println("additional data");
    for (byte bi : additionalData)
      System.out.format("0x%x ", bi);
    System.out.println(""); // debug
    */

    byte[] decryptedPayload;

    try {
      decryptedPayload = CCMBlockCipher.decrypt(pairwiseKey, nonce, additionalData, msg.getPayload(), SMACKAuthFieldSize);
    }
    catch (Exception e) {
      LOGGER.warning("Bad secured message from " + msg.getSource().toString() + " : " + e.getMessage()); //Rikard
      return false;
    }

    LOGGER.info(String.format("Received authentic message, length: %d", decryptedPayload.length)); // debug

    // Replace the secured payload with the plain one
    if (decryptedPayload.length == 0)
      msg.setPayload((byte[]) null);
    else
      msg.setPayload(decryptedPayload);

    return true;

  }

}
