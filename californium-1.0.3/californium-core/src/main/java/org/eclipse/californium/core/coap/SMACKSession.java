package org.eclipse.californium.SMACKEnvironment;

import java.util.Arrays;
//import org.eclipse.californium.coap.EndpointAddress; //Disabled //Rikard
import org.eclipse.californium.core.network.config.NetworkConfig;
import java.lang.CloneNotSupportedException;

/* Session are organized in a map structure, and uniquely identified by the initial message ID */
/* Such a value is used as the key to retrieve the correct map entry */

public class SMACKSession {

  private NetworkConfig config = NetworkConfig.getStandard();
  
  protected int keySize = config.getInt(NetworkConfig.Keys.SMACK_KEY_SIZE);
  
  protected byte[] KS; // Session key (received by KDC)
  
  protected int lastID; // Last used (next) message ID on the server (client) side
    
  public SMACKSession(int firstID, byte[] sessionKey) {
  
    KS = new byte[keySize];
    System.arraycopy(sessionKey, 0, KS, 0, sessionKey.length);
    lastID = firstID;
  }
  
  public int getLastID() { return lastID; }
 
  public byte[] getSessionKey() { return KS; }
 
  public void incrementLastID() {
    
    int maxID = config.getInt(NetworkConfig.Keys.SMACK_MAX_MESSAGE_ID);
    lastID = (lastID == maxID) ? 0 : lastID + 1;
    
  }
 
}
