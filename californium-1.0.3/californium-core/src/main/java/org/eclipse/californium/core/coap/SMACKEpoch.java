package org.eclipse.californium.coap.SMACKepoch;

import org.eclipse.californium.core.network.config.NetworkConfig;
import java.util.Set;
import java.util.HashSet;
import java.lang.Math;

public class SMACKEpoch {

  private NetworkConfig config = NetworkConfig.getStandard();

  private boolean acceptFutureSessions = true; // if true, new "future" initial message ID are acccepted
  private boolean active = false; // true it the epoch has actually started
  private int A; // Acceptance window size (in SMACK sessions)
  private int T; // Window update step (in SMACK sessions)
  private int oldestSessionInitialID; // Initial message ID of the oldest active SMACK session
  private Set<Integer> activeSessionInitialIDs; // Set of initial message IDs in the current SMACK epoch
  private int firstID; // First initial message ID in the current SMACK epoch
  private int finalID; // Last admitted initial message ID in the current session epoch
  
  public SMACKEpoch(int aws, int wup, int ID) {
  
    A = aws;
    T = wup;
    firstID = ID;
    activeSessionInitialIDs = new HashSet<Integer>();
    
  }
  
  public boolean getAcceptFutureSessions() { return acceptFutureSessions; }
  
  public boolean getActive() { return active; }
  
  public int getAcceptanceWindowSize() { return A; }
  
  public int getWindowUpdateStep() { return T; }
  
  public int getOldestSessionInitialID() { return oldestSessionInitialID; }
  
  public int getFirstID() { return firstID; }
  
  public int getFinalID() { return finalID; }
  
  public boolean isSessionActive(int ID) { return activeSessionInitialIDs.contains(ID); }
  
  public void setAcceptFutureSessions(boolean v) { acceptFutureSessions = v; }
  
  public void setActive(boolean v) { active = v; }
   
  public void setOldestSessionInitialID(int ID) { oldestSessionInitialID = ID; }
  
  public void addActiveSessionInitialIDs(int ID) { activeSessionInitialIDs.add(ID); }
  
  public void removeActiveSessionInitialIDs(int ID) { activeSessionInitialIDs.remove(ID); }
  
  public void startNewEpoch(int ID) {
  
    active = true;
    oldestSessionInitialID = ID;
    activeSessionInitialIDs.add(new Integer(ID));
    
    int p = config.getInt(NetworkConfig.Keys.SMACK_SESSION_LENGTH);
    
    if (ID > A * p) {
      finalID = ID - (A * p);
    }
    else {
      finalID = (int)(Math.pow(2, 16)) - ((A * p) - ID);
    }
  
  }

}
