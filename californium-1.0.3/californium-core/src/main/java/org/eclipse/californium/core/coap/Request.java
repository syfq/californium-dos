/*******************************************************************************
 * Copyright (c) 2015 Institute for Pervasive Computing, ETH Zurich and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 *
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *    http://www.eclipse.org/org/documents/edl-v10.html.
 *
 * Contributors:
 *    Matthias Kovatsch - creator and main architect
 *    Martin Lanter - architect and re-implementation
 *    Dominique Im Obersteg - parsers and initial implementation
 *    Daniel Pauli - parsers and initial implementation
 *    Kai Hudalla (Bosch Software Innovations GmbH) - logging
 *    Kai Hudalla (Bosch Software Innovations GmbH) - add field for sender identity
 *                                                    (465073)
 *    Achim Kraus (Bosch Software Innovations GmbH) - move payload string conversion
 *    												  from toString() to
 *                                                    Message.getPayloadTracingString().
 *                                                    (for message tracing)
 ******************************************************************************/
package org.eclipse.californium.core.coap;

import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.security.Principal;
import java.nio.ByteBuffer;

import org.eclipse.californium.core.coap.CoAP.Code;
import org.eclipse.californium.core.coap.CoAP.Type;
import org.eclipse.californium.core.network.Endpoint;
import org.eclipse.californium.core.network.EndpointManager;
import org.eclipse.californium.SMACKEnvironment.SMACKClientEnvironment; // M.T.
import org.eclipse.californium.SMACKEnvironment.SMACKClientSession; //Rikard
import java.util.HashMap; // M.T.
import org.apache.commons.lang.builder.ToStringBuilder; // debugging purpose //Syafiq
import org.apache.commons.lang.builder.ToStringStyle; // debugging purpose //Syafiq
import org.eclipse.californium.core.network.config.NetworkConfig; //Rikard
import org.eclipse.californium.core.network.stack.SecurityBox;
import org.eclipse.californium.scandium.config.DtlsConnectorConfig; // syafiq
import org.eclipse.californium.scandium.config.DtlsConnectorConfig.Builder; // syafiq
import org.eclipse.californium.scandium.DTLSConnector; // syafiq
import org.eclipse.californium.scandium.dtls.pskstore.StaticPskStore; // syafiq
import java.net.InetSocketAddress;
import org.eclipse.californium.core.network.CoapEndpoint;
import java.util.Arrays;

/**
 * Request represents a CoAP request and has either the {@link Type} CON or NON
 * and one of the {@link CoAP.Code}s GET, POST, PUT or DELETE. A request must be
 * sent over an {@link Endpoint} to its destination. By default, a request
 * chooses the default endpoint defined in {@link EndpointManager}. The server
 * responds with a {@link Response}. The client can wait for such a response
 * with a synchronous call, for instance:
 *
 * <pre>
 * Request request = new Request(Code.GET);
 * request.setURI(&quot;coap://example.com:5683/sensors/temperature&quot;);
 * request.send();
 * Response response = request.waitForResponse();
 * </pre>
 *
 * The client can also send requests asynchronously and define a handler that is
 * invoked when a response arrives. This is in particular useful, when a client
 * wants to observe the target resource and react to notifications. For
 * instance:
 *
 * <pre>
 * Request request = new Request(Code.GET);
 * request.setURI(&quot;coap://example.com:5683/sensors/temperature&quot;);
 * request.setObserve();
 *
 * request.addMessageObserver(new MessageObserverAdapter() {
 *   public void responded(Response response) {
 *     if (response.getCode() == ResponseCode.CONTENT) {
 *       System.out.println(&quot;Received &quot; + response.getPayloadString());
 *     } else {
 *       // error handling
 *     }
 *   }
 * });
 * request.send();
 * </pre>
 *
 * We can also modify the options of a request. For example:
 *
 * <pre>
 * Request post = new Request(Code.POST);
 * post.setPayload("Plain text");
 * post.getOptions()
 *   .setContentFormat(MediaTypeRegistry.TEXT_PLAIN)
 *   .setAccept(MediaTypeRegistry.TEXT_PLAIN)
 *   .setIfNoneMatch(true);
 * String response = post.send().waitForResponse().getPayloadString();
 * </pre>
 * @see Response
 */
public class Request extends Message {

	/** The request code. */
	private final CoAP.Code code;

	/** Marks this request as multicast request */
	private boolean multicast;

	/** The current response for the request. */
	private Response response;
	private Response realResponse; // syafiq debug
	private Response RetransmittedResponse;
	private Response ProtectedResponse;
	private Response OffResponse;
	private Response tempResponse;
	private Response responseOff;

	private String scheme;

	/** The lock object used to wait for a response. */
	private Object lock;

	/** the authenticated (remote) sender's identity **/
	private Principal senderIdentity;

	private static NetworkConfig config = NetworkConfig.getStandard(); //Rikard
	private static int currentSessionID = config.getInt(NetworkConfig.Keys.SMACK_EPOCH_INITIAL_ID);
	public static int SessionLen = config.getInt(NetworkConfig.Keys.SMACK_SESSION_LENGTH);
	private static String identity = "Client_identity"; // syafiq
  private static String psk = "secretPSK"; // syafiq
	private static int SMACKMaxMID = config.getInt(NetworkConfig.Keys.SMACK_MAX_MESSAGE_ID);

	// syafiq
	// indices of command line parameters
	private static final int IDX_METHOD          = 0;
	private static final int IDX_URI             = 1;
	private static final int IDX_PAYLOAD         = 2;

	// exit codes for runtime errors
	private static final int ERR_MISSING_METHOD  = 1;
	private static final int ERR_UNKNOWN_METHOD  = 2;
	private static final int ERR_MISSING_URI     = 3;
	private static final int ERR_BAD_URI         = 4;
	private static final int ERR_REQUEST_FAILED  = 5;
	private static final int ERR_RESPONSE_FAILED = 6;
	private static final int ERR_BAD_LINK_FORMAT = 7;

	private static byte[] State_NP_ServerToClient = new byte[] { 0x01, 0x00, };
	private static byte[] State_NP_ServerToKDC 	 	= new byte[] { 0x01, 0x01, };
	private static byte[] State_NP_KDCToServer 	 	= new byte[] { 0x01, 0x02, };
	private static byte[] State_PO_ServerToKDC 	 	= new byte[] { 0x01, 0x03, };
	private static byte[] State_PO_KDCToServer 	 	= new byte[] { 0x01, 0x04, };
	private static byte[] State_PO_KDCToClient	 	= new byte[] { 0x01, 0x05, };
	private static byte[] State_OP_ServerToKDC 	 	= new byte[] { 0x01, 0x06, };
	private static byte[] State_OP_KDCToServer 	 	= new byte[] { 0x01, 0x07, };
	private static byte[] State_OP_KDCToClient 	 	= new byte[] { 0x01, 0x08, };
	private static byte[] State_PN_ServerToKDC 	 	= new byte[] { 0x01, 0x09, };
	private static byte[] State_PN_KDCToServer 	 	= new byte[] { 0x01, 0x0a, };
	private static byte[] State_PN_KDCToClient 	 	= new byte[] { 0x01, 0x0b, };

	private static byte[] SecureTraffic_Protected = new byte[] { 0x02, 0x00 };
	private static byte[] SecureTraffic_Off				= new byte[] { 0x02, 0x01 };
	private static byte[] TimeInfo_Off						= new byte[] { 0x02, 0x02 };
	private static byte[] ACK_Cached_Request			= new byte[] { 0x02, 0x03 };

	private static boolean isServer = config.getBoolean(NetworkConfig.Keys.IS_SERVER);
	private static boolean isClient = config.getBoolean(NetworkConfig.Keys.IS_CLIENT);
	private static boolean isProxy  = config.getBoolean(NetworkConfig.Keys.IS_PROXY);

	private static String ClientAddr = config.getString(NetworkConfig.Keys.CLIENT_ADDRESS);
	private static String ProxyAddr  = config.getString(NetworkConfig.Keys.PROXY_ADDRESS);
	private static String ServerAddr = config.getString(NetworkConfig.Keys.SERVER_ADDRESS);
	private static String ProxySecurePort = config.getString(NetworkConfig.Keys.PROXY_SECURE_PORT);
  private static String ServerSecurePort = config.getString(NetworkConfig.Keys.SERVER_SECURE_PORT);
  private static String ServerPort = config.getString(NetworkConfig.Keys.SERVER_PORT);

	private static int TOffC = config.getInt(NetworkConfig.Keys.T_OFF_C);

	//public static Request offReq = newGet();

	/**
	 * Instantiates a new request with the specified CoAP code and no (null)
	 * message type.
	 *
	 * @param code the request code
	 */
	public Request(Code code) {
		super();
		this.code = code;
	}

	/**
	 * Instantiates a new request with the specified CoAP code and message type.
	 *
	 * @param code the request code
	 * @param type the message type
	 */
	public Request(Code code, Type type) {
		super(type);
		this.code = code;
	}

	/**
	 * Gets the request code.
	 *
	 * @return the code
	 */
	public Code getCode() {
		return code;
	}

	/**
	 * Gets the scheme.
	 *
	 * @return the scheme
	 */
	public String getScheme() {
		return scheme;
	}

	/**
	 * Sets the scheme.
	 *
	 * @param scheme the new scheme
	 */
	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	/**
	 * Tests if this request is a multicast request
	 *
	 * @return true if this request is a multicast request.
	 */
	public boolean isMulticast() {
		return multicast;
	}

	/**
	 * Defines whether this request is a multicast request or not.
	 *
	 * @param multicast if this request is a multicast request
	 */
	public void setMulticast(boolean multicast) {
		this.multicast = multicast;
	}

	/**
	 * {@inheritDoc}
	 *
	 * Required in Request to keep class for fluent API.
	 */
	public Request setPayload(String payload) {
		super.setPayload(payload);
		return this;
	}

	/**
	 * {@inheritDoc}
	 *
	 * Required in Request to keep class for fluent API.
	 */
	public Request setPayload(byte[] payload) {
		super.setPayload(payload);
		return this;
	}

	/**
	 * This is a convenience method to set the reques's options for host, port
	 * and path with a string of the form
	 * <code>[scheme]://[host]:[port]{/resource}*?{&amp;query}*</code>
	 *
	 * @param uri the URI defining the target resource
	 * @return this request
	 */
	public Request setURI(String uri) {
		try {
			if (!uri.startsWith("coap://") && !uri.startsWith("coaps://"))
				uri = "coap://" + uri;
			return setURI(new URI(uri));
		} catch (URISyntaxException e) {
			throw new IllegalArgumentException("Failed to set uri "+uri + ": " + e.getMessage());
		}
	}

	/**
	 * This is a convenience method to set the request's options for host, port
	 * and path with a URI object.
	 *
	 * @param uri the URI defining the target resource
	 * @return this request
	 */
	public Request setURI(URI uri) {
		/*
		 * Implementation from old Cf from Dominique Im Obersteg, Daniel Pauli
		 * and Francesco Corazza.
		 */
		String host = uri.getHost();
		// set Uri-Host option if not IP literal
		if (host != null && !host.toLowerCase().matches("(\\[[0-9a-f:]+\\]|[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3})")) {
			if (!host.equals("localhost"))
				getOptions().setUriHost(host);
		}

		try {
			setDestination(InetAddress.getByName(host));
		} catch (UnknownHostException e) {
			throw new IllegalArgumentException("Failed to set unknown host "+host);
    	}

		String scheme = uri.getScheme();
		if (scheme != null) {
			// decide according to URI scheme whether DTLS is enabled for the client
			this.scheme = scheme;
		}

		/*
		 * The Uri-Port is only for special cases where it differs from the UDP port,
		 * usually when Proxy-Scheme is used.
		 */
		int port = uri.getPort();
		if (port >= 0) {
			if (port != CoAP.DEFAULT_COAP_PORT)
				getOptions().setUriPort(port);
			setDestinationPort(port);
		} else if (getDestinationPort() == 0) {
			if (scheme == null || scheme.equals(CoAP.COAP_URI_SCHEME))
				setDestinationPort(CoAP.DEFAULT_COAP_PORT);
			else if (scheme.equals(CoAP.COAP_SECURE_URI_SCHEME))
				setDestinationPort(CoAP.DEFAULT_COAP_SECURE_PORT);
		}

		// set Uri-Path options
		String path = uri.getPath();
		if (path != null && path.length() > 1) {
			getOptions().setUriPath(path);
		}

		// set Uri-Query options
		String query = uri.getQuery();
		if (query != null) {
			getOptions().setUriQuery(query);
		}
		return this;
	}

	// TODO: test this method.
	/**
	 * Returns the absolute Request-URI as string.
	 * To support virtual servers, it either uses the Uri-Host option
	 * or "localhost" if the option is not present.
	 * @return the absolute URI string
	 */
	public String getURI() {
		StringBuilder builder = new StringBuilder();
		String scheme = getScheme();
		if (scheme != null) builder.append(scheme).append("://");
		else builder.append("coap://");
		String host = getOptions().getUriHost();
		if (host != null) builder.append(host);
		else builder.append("localhost");
		Integer port = getOptions().getUriPort();
		if (port != null) builder.append(":").append(port);
		String path = getOptions().getUriPathString();
		builder.append("/").append(path);
		String query = getOptions().getUriQueryString();
		if (query.length()>0) builder.append("?").append(query);
		// TODO: Query as well?
		return builder.toString();
	}

	/**
	 * Gets the authenticated (remote) sender's identity.
	 *
	 * @return the identity or <code>null</code> if the sender has
	 *             not been authenticated
	 */
	public Principal getSenderIdentity() {
		return this.senderIdentity;
	}

	/**
	 * Sets the authenticated (remote) sender's identity.
	 *
	 * This method is invoked by <em>Californium</em> when receiving
	 * a request from a client in order to include the client's
	 * authenticated identity. It has no effect on outbound
	 * requests sent to other CoAP servers. In particular,
	 * it has no impact on a DTLS handshake (potentially) taking
	 * place with that server.
	 *
	 * @param senderIdentity the identity
	 * @return this request
	 */
	public Request setSenderIdentity(Principal senderIdentity) {
		this.senderIdentity = senderIdentity;
		return this;
	}

	/**
	 * Sends the request over the default endpoint to its destination and
	 * expects a response back.
	 * @return this request
	 */
	public Request send() {
		validateBeforeSending();
		if (CoAP.COAP_SECURE_URI_SCHEME.equals(getScheme())) {
			// This is the case when secure coap is supposed to be used
			EndpointManager.getEndpointManager().getDefaultSecureEndpoint().sendRequest(this);
		} else {
			// This is the normal case
			EndpointManager.getEndpointManager().getDefaultEndpoint().sendRequest(this);
		}
		return this;
	}

	//Method for accessing SMACK client session of request //Rikard
	public SMACKClientSession getClientSession()
	{
		System.out.println("DEBUG: client session on request.java"); // debug syafiq
		return clientSession;
	}

	private SMACKClientSession clientSession;
	public static Integer count = 0; // initial request already counted here
	public static Integer curSessionID;
	// M.T. (This is invoked to send Request messages only) //TODO: Rename? //Rikard
	public void execute(SMACKClientEnvironment clientEnvironment, int index) {
		// TODO reset response, requires new MID

		//sendSMACK(clientEnvironment, index); //Changed this to below //Rikard
		SMACKClientSession s = clientEnvironment.getSession(index);
		clientSession = s; //Stores the SMACK client session in this Request
		if (true) {//(msg != null) {
			//doSendMessageSMACK(msg, s); //Changed this to below //Rikard
			send();
		}
	}

	// main request execution of request message from the client - syafiq
	public void AllExecute() {
		//System.out.println("SessionLen ="+SessionLen);
		//System.out.println("count ="+count);
		//System.out.println("count%SessionLen ="+(count%SessionLen));
		if ((count%SessionLen) == 0 && count == 0) {
			// first time contact the KDC

			//System.out.println("This is InitialExecute && count = 0");
			InitialExecute();
		} else if (((count%SessionLen==0 && count!=0) || count==SMACKMaxMID+1) && SecurityBox.getIsClientNormal()) {
			// if the request is reaching 127 messages, need to renew SMACK material

			//System.out.println("This is InitialExecute && count != 0 || (count == SMACKMaxMID+1) && SecurityBox.getIsClientNormal()");
			UpdateSMACKMaterialExecute();
		} else {
			// The rest of the execution
			//System.out.println("This is ContinuingExecute");
			ContinuingExecute();
		}
		count++;
	}

	// first time the client contact the KDC - syafiq
	public void InitialExecute() {
		// if state is unknown, it is assumed to be normal
		SecurityBox.setClientStateNormal();
		InetSocketAddress UnsecureBindToAddress = new InetSocketAddress(ClientAddr, 0);
		EndpointManager.getEndpointManager().setDefaultEndpoint(new CoapEndpoint(UnsecureBindToAddress));
		Endpoint defaultEndpoint = EndpointManager.getEndpointManager().getDefaultEndpoint();

		byte[] sessionKey = defaultEndpoint.getSessionKey();
		int sessionID = defaultEndpoint.getSessionID();

		SMACKClientEnvironment.addSession(sessionID, sessionKey);
		SMACKClientSession s = SMACKClientEnvironment.getSession(sessionID);
		byte[] tokenPass = this.getToken();
		clientSession = s;
		if (true) {
			send(defaultEndpoint);
		}

		try {
			response = this.waitForResponse();
		} catch (Exception e) {
			System.err.println("Failed to receive response: " + e.getMessage());
			System.exit(ERR_RESPONSE_FAILED);
		}

		// going to protected state
		if (this.isTimedOut()) {
			System.out.println("going to protected state due to time out");
			SecurityBox.setClientStateProtected();
			//this.response = ProtectedExecute();
			this.response = RetransmitInitialRequest(sessionID, tokenPass);
		// going to protected state
		} else if (Arrays.equals(response.getPayload(), State_NP_ServerToClient) && isClient && !this.isTimedOut()) {
			System.out.println("going to protected state due to receive notification from server");
			SecurityBox.setClientStateProtected();
			this.response = RetransmitInitialRequest(sessionID, tokenPass);
		// continue with normal state
		} else {
			this.response = response;
		}
	}

	// after the SMACK material is obtained - syafiq
	public void ContinuingExecute() {
		// normal state
		if (SecurityBox.getIsClientNormal() && !SecurityBox.getIsClientProtected() && !SecurityBox.getIsClientOff()) {
			System.out.println("running normal state");
			curSessionID = SMACKClientEnvironment.getCurrentSessionID(SecurityBox.SessionID);
			SMACKClientSession s = SMACKClientEnvironment.getSession(curSessionID);
			clientSession = s;
			if (true) {
				send();
			}

			try {
				response = this.waitForResponse();
			} catch (Exception e) {
				System.err.println("Failed to receive response: " + e.getMessage());
				System.exit(ERR_RESPONSE_FAILED);
			}
			byte[] tokenPass = this.getToken();

			// FIXME!
			// going to off state
			if (this.isTimedOut()) {
				System.out.println("Set the flag of using dtls to ON");
				//SecurityBox.setClientStateOff();
				//this.response = RetransmitInitialRequest(curSessionID, tokenPass);
				SecurityBox.setClientStateProtected();
				//this.response = ProtectedExecute();
				this.response = RetransmitInitialRequest(curSessionID, tokenPass);
			// going to protected state
			} else if (Arrays.equals(response.getPayload(), State_NP_ServerToClient) && isClient && !this.isTimedOut()) {
				System.out.println("Set the flag of using dtls to ON");
				SecurityBox.setClientStateProtected();
				this.response = RetransmitInitialRequest(curSessionID, tokenPass);
			// continue with normal state
			} else {
				this.response = response;
			}

		// protected state
		} else if (!SecurityBox.getIsClientNormal() && SecurityBox.getIsClientProtected() && !SecurityBox.getIsClientOff()) {
			System.out.println("running protected state");

			tempResponse = ProtectedExecute();

			if (Arrays.equals(tempResponse.getPayload(), State_PN_KDCToClient) && isClient) {
				System.out.println("Back to Normal State");
				SecurityBox.setClientStateNormal();
			} else if (Arrays.equals(tempResponse.getPayload(), State_PO_KDCToClient) && isClient) {
				System.out.println("Go to Off State");
				System.out.println("payload received ="+ToStringBuilder.reflectionToString(tempResponse.getPayload()));
				SecurityBox.setClientStateOff();
				responseOff = OffExecute(TOffC);
				System.out.println("request off"+this);
				System.out.println("response off"+responseOff);
				byte[] payloadOff = Arrays.copyOfRange(responseOff.getPayload(), 0, 2);
				if (Arrays.equals(payloadOff, TimeInfo_Off)) {
					byte[] ArrayWaitTimeServer = Arrays.copyOfRange(responseOff.getPayload(), 2, 6);
					Integer WaitTimeServer = ByteBuffer.wrap(ArrayWaitTimeServer).getInt();
					int newWaitTime = WaitTimeServer+5;
					responseOff = OffExecute(newWaitTime);
					this.response = responseOff;
					//System.out.println("do something!");
				} else {
					this.response = responseOff;
				}
				SecurityBox.setClientStateProtected();
			} else {
				this.response = tempResponse;
			}

		// off state
		//} else if (!SecurityBox.getIsClientNormal() && !SecurityBox.getIsClientProtected() && SecurityBox.getIsClientOff()) {
		//	System.out.println("running off state");
		//	responseOff = OffExecute(130000);
		//	this.response = responseOff;
		//	//SecurityBox.setClientStateProtected();
		//	//if (Arrays.equals(response.getPayload(), State_OP_KDCToClient) && isClient) {
		//	//	System.out.println("Back to Protected State");
		//	//	SecurityBox.setClientStateProtected();
		//	//}

		} else {
			System.out.println("Undefined state!"); // this should never happen
			//SecurityBox.setClientStateProtected();
		}
	}

	// if the client receive a response with signalling information to change to other state, the actual request should be retransmitted - syafiq
	public Response RetransmitInitialRequest(int sessionID, byte[] tokenPass) {
		System.out.println("Set the flag of using dtls to ON");

		Request newReq = newGet();
		newReq.setPayload((byte[]) SecureTraffic_Protected);
		newReq.setMID(sessionID);
		newReq.setURI("coaps://"+ProxyAddr+":"+ProxySecurePort+"/coap2coap");
		newReq.setType(Type.CON);
		newReq.setToken(this.getToken());
		newReq.getOptions().setProxyUri("coaps://"+ServerAddr+":"+ServerSecurePort+"/helloWorld");
		newReq.getOptions().setContentFormat(MediaTypeRegistry.TEXT_PLAIN);

		System.out.println("Send packet retransmit :"+newReq);
		if (EndpointManager.getEndpointManager().getDefaultSecureEndpoint() == null) {
			EndpointManager.getEndpointManager().setDefaultSecureEndpoint(SecurityBox.ClientDTLSEndpoint);
		}
		EndpointManager.getEndpointManager().getDefaultSecureEndpoint().sendRequest(newReq);

		try {
			RetransmittedResponse = newReq.waitForResponse();
		} catch (Exception e) {
			System.err.println("Failed to receive response: " + e.getMessage());
			System.exit(ERR_RESPONSE_FAILED);
		}

		RetransmittedResponse.setToken(tokenPass);
		SecurityBox.setClientStateProtected();
		return RetransmittedResponse;
	}

	// The client moves to protected state, and act accordingling - syafiq
	public Response ProtectedExecute() {
		this.setURI("coaps://"+ProxyAddr+":"+ProxySecurePort+"/coap2coap");
		this.setPayload((byte[]) SecureTraffic_Protected);
		this.setType(Type.CON);
		this.getOptions().setProxyUri("coaps://"+ServerAddr+":"+ServerSecurePort+"/helloWorld");
		this.getOptions().setContentFormat(MediaTypeRegistry.TEXT_PLAIN);

		if (EndpointManager.getEndpointManager().getDefaultSecureEndpoint() == null) {
			EndpointManager.getEndpointManager().setDefaultSecureEndpoint(SecurityBox.ClientDTLSEndpoint);
		}
		EndpointManager.getEndpointManager().getDefaultSecureEndpoint().sendRequest(this);

		try {
			ProtectedResponse = this.waitForResponse();
		} catch (Exception e) {
			System.err.println("Failed to receive response: " + e.getMessage());
			System.exit(ERR_RESPONSE_FAILED);
		}

		return ProtectedResponse;
	}

	// The client moves to OFF state, and act accordingling - syafiq
	public Response OffExecute(int waitingTime) {
		Request offReq = newGet();
		offReq.setURI("coaps://"+ProxyAddr+":"+ProxySecurePort+"/coap2coap");
		byte[] OffTime = ByteBuffer.allocate(4).putInt(waitingTime).array();
		byte[] payloadOff = new byte[SecureTraffic_Off.length + OffTime.length];
		System.arraycopy(SecureTraffic_Off, 0, payloadOff, 0, SecureTraffic_Off.length);
		System.arraycopy(OffTime, 0, payloadOff, SecureTraffic_Off.length, OffTime.length);
		offReq.setPayload(payloadOff);
		offReq.setType(Type.CON);
		offReq.setToken(this.getToken());
		offReq.getOptions().setProxyUri("coaps://"+ServerAddr+":"+ServerSecurePort+"/helloWorld");
		offReq.getOptions().setContentFormat(MediaTypeRegistry.TEXT_PLAIN);

		if (EndpointManager.getEndpointManager().getDefaultSecureEndpoint() == null) {
			EndpointManager.getEndpointManager().setDefaultSecureEndpoint(SecurityBox.ClientDTLSEndpoint);
		}
		EndpointManager.getEndpointManager().getDefaultSecureEndpoint().sendRequest(offReq);

		try {
			//OffResponse = offReq.waitForResponse(1);
			OffResponse = offReq.waitForResponse(waitingTime*1000);
		} catch (Exception e) {
			System.err.println("Failed to receive response: " + e.getMessage());
			System.exit(ERR_RESPONSE_FAILED);
		}
		System.out.println("OffResponse ="+OffResponse);
		return OffResponse;
	}

	// to update smack material (after 127 requests) - syafiq
	public void UpdateSMACKMaterialExecute() {
		// Ask for renewed SMACK material
		SecurityBox.runClientInitCont();

		Endpoint defaultEndpoint = EndpointManager.getEndpointManager().getDefaultEndpoint();

		byte[] sessionKey = SecurityBox.ClientSessionKey;
		int sessionID = SecurityBox.SessionID;

		System.out.println("sessionKey = "+sessionKey);
		System.out.println("sessionID = "+sessionID);
		SMACKClientEnvironment.addSession(sessionID, sessionKey);

		SMACKClientSession s = SMACKClientEnvironment.getSession(sessionID);
		System.out.println("s ="+ToStringBuilder.reflectionToString(s));
		clientSession = s;
		if (true) {
			send(defaultEndpoint);
		}
	}

	// only for cached requests - syafiq
	public Request sendCachedReq(Endpoint endpoint) {
		endpoint.sendRequestCached(this);
		return this;
	}

	/**
	 * Sends the request over the specified endpoint to its destination and
	 * expects a response back.
	 *
	 * @param endpoint the endpoint
	 * @return this request
	 */
	public Request send(Endpoint endpoint) {
		validateBeforeSending();
		endpoint.sendRequest(this);
		return this;
	}

	/**
	 * Validate before sending that there is a destination set.
	 */
	private void validateBeforeSending() {
		if (getDestination() == null)
			throw new NullPointerException("Destination is null");
		if (getDestinationPort() == 0)
			throw new NullPointerException("Destination port is 0");
	}

	/**
	 * Sets CoAP's observe option. If the target resource of this request
	 * responds with a success code and also sets the observe option, it will
	 * send more responses in the future whenever the resource's state changes.
	 *
	 * @return this Request
	 */
	public Request setObserve() {
		getOptions().setObserve(0);
		return this;
	}

	/**
	 * Sets CoAP's observe option to the value of 1 to proactively cancel.
	 *
	 * @return this Request
	 */
	public Request setObserveCancel() {
		getOptions().setObserve(1);
		return this;
	}

	/**
	 * Gets the response or null if none has arrived yet.
	 *
	 * @return the response
	 */
	public Response getResponse() {
		return response;
	}

	/**
	 * Sets the response.
	 *
	 * @param response
	 *            the new response
	 */
	public void setResponse(Response response) {
		System.out.println("response set ="+response);
		this.response = response;
		// only for synchronous/blocking requests
		System.out.println("lock = "+lock);
		if (lock != null) {
			synchronized (lock) {
				lock.notifyAll();
			}
		}
		// else: we know that nobody is waiting on the lock

		for (MessageObserver handler:getMessageObservers())
			handler.onResponse(response);
	}

	/**
	 * Wait for the response. This function blocks until there is a response or
	 * the request has been canceled.
	 *
	 * @return the response
	 * @throws InterruptedException
	 *             the interrupted exception
	 */
	public Response waitForResponse() throws InterruptedException {
		return waitForResponse(0);
	}

	/**
	 * Wait for the response. This function blocks until there is a response,
	 * the request has been canceled or the specified timeout has expired. A
	 * timeout of 0 is interpreted as infinity. If a response is already here,
	 * this method returns it immediately.
	 * <p>
	 * The calling thread returns if either a response arrives, the request gets
	 * rejected by the server, the request gets canceled or, in case of a
	 * confirmable request, timeouts. In that case, if no response has arrived
	 * yet the return value is null.
	 * <p>
	 * This method also sets the response to null so that succeeding calls will
	 * wait for the next response. Repeatedly calling this method is useful if
	 * the client expects multiple responses, e.g., multiple notifications to an
	 * observe request or multiple responses to a multicast request.
	 *
	 * @param timeout the maximum time to wait in milliseconds.
	 * @return the response (null if timeout occurred)
	 * @throws InterruptedException the interrupted exception
	 */
	public Response waitForResponse(long timeout) throws InterruptedException {
		long before = System.currentTimeMillis();
		long expired = timeout>0 ? (before + timeout) : 0;
		// Lazy initialization of a lock
		if (lock == null) {
			synchronized (this) {
				if (lock == null)
					lock = new Object();
			}
		}
		// wait for response
		synchronized (lock) {
			while (this.response == null && !isCanceled() && !isTimedOut() && !isRejected()) {
				lock.wait(timeout);
				long now = System.currentTimeMillis();
				// timeout expired?
				if (timeout > 0 && expired <= now) {
					// break loop since response is still null
					break;
				}
			}
			Response r = this.response;
			this.response = null;
			return r;
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * Furthermore, if the request is canceled, it will wake up all threads that
	 * are currently waiting for a response.
	 */
	@Override
	public void setTimedOut(boolean timedOut) {
		super.setTimedOut(timedOut);
		if (timedOut && lock != null) {
			synchronized (lock) {
				lock.notifyAll();
			}
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * Furthermore, if the request is canceled, it will wake up all threads that
	 * are currently waiting for a response.
	 */
	@Override
	public void setCanceled(boolean canceled) {
		super.setCanceled(canceled);
		if (canceled && lock != null) {
			synchronized (lock) {
				lock.notifyAll();
			}
		}
	}

	@Override
	public void setRejected(boolean rejected) {
		super.setRejected(rejected);
		if (rejected  && lock != null) {
			synchronized (lock) {
				lock.notifyAll();
			}
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String payload = getPayloadTracingString();
		return String.format("%s-%-6s MID=%5d, Token=%s, OptionSet=%s, %s", getType(), getCode(), getMID(), getTokenString(), getOptions(), payload);
	}

	////////// Some static factory methods for convenience //////////

	/**
	 * Convenience factory method to construct a GET request and equivalent to
	 * <code>new Request(Code.GET);</code>
	 *
	 * @return a new GET request
	 */
	public static Request newGet() { return new Request(Code.GET); }

	/**
	 * Convenience factory method to construct a POST request and equivalent to
	 * <code>new Request(Code.POST);</code>
	 *
	 * @return a new POST request
	 */
	public static Request newPost() { return new Request(Code.POST); }

	/**
	 * Convenience factory method to construct a PUT request and equivalent to
	 * <code>new Request(Code.PUT);</code>
	 *
	 * @return a new PUT request
	 */
	public static Request newPut() { return new Request(Code.PUT); }

	/**
	 * Convenience factory method to construct a DELETE request and equivalent
	 * to <code>new Request(Code.DELETE);</code>
	 *
	 * @return a new DELETE request
	 */
	public static Request newDelete() { return new Request(Code.DELETE); }

}
