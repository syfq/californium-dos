/*******************************************************************************
 * Copyright (c) 2015 Institute for Pervasive Computing, ETH Zurich and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 *
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *    http://www.eclipse.org/org/documents/edl-v10.html.
 *
 * Contributors:
 *    Matthias Kovatsch - creator and main architect
 *    Kai Hudalla (Bosch Software Innovations GmbH) - add endpoints for all IP addresses
 ******************************************************************************/
package org.eclipse.californium.examples;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.network.EndpointManager;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.core.server.resources.CoapExchange;

//syafiq
import org.eclipse.californium.core.network.stack.SecurityBox;

public class HelloWorldServer extends CoapServer {

	private static NetworkConfig config = NetworkConfig.getStandard(); //Rikard
	private static final int COAP_PORT = config.getInt(NetworkConfig.Keys.COAP_PORT);
	private static final String ClientAddr = config.getString(NetworkConfig.Keys.CLIENT_ADDRESS);
	private static final String ProxyAddr  = config.getString(NetworkConfig.Keys.PROXY_ADDRESS);
	private static final String ServerAddr = config.getString(NetworkConfig.Keys.SERVER_ADDRESS);
  private static final int ProxySecurePort = config.getInt(NetworkConfig.Keys.PROXY_SECURE_PORT);
  private static final int ServerSecurePort = config.getInt(NetworkConfig.Keys.SERVER_SECURE_PORT);
  private static final int ServerPort = config.getInt(NetworkConfig.Keys.SERVER_PORT);

    /*
     * Application entry point.
     */
    public static void main(String[] args) {
			try {
				HelloWorldServer server = new HelloWorldServer();
				server.addEndpoints();
				server.start();
			} catch (SocketException e) {
				System.err.println("Failed to initialize server: " + e.getMessage());
			}
    }

    /**
     * Add individual endpoints listening on default CoAP port on all IPv4 addresses of all network interfaces.
     */
    private void addEndpoints() {
    	for (InetAddress addr : EndpointManager.getEndpointManager().getNetworkInterfaces()) {
    		// only binds to IPv4 addresses and localhost
				if (addr instanceof Inet4Address && addr.isLoopbackAddress()) {
					// unsecure listener
					InetSocketAddress UnsecureBindToAddress = new InetSocketAddress(ServerAddr, ServerPort);
					addEndpoint(new CoapEndpoint(UnsecureBindToAddress));
					addEndpoint(SecurityBox.ServerDTLSEndpoint);
				}
			}
    }

    /*
     * Constructor for a new Hello-World server. Here, the resources
     * of the server are initialized.
     */
    public HelloWorldServer() throws SocketException {

        // provide an instance of a Hello-World resource
        add(new HelloWorldResource());
    }

    /*
     * Definition of the Hello-World Resource
     */
    class HelloWorldResource extends CoapResource {

        public HelloWorldResource() {

            // set resource identifier
            super("helloWorld");

            // set display name
            getAttributes().setTitle("Hello-World Resource");
        }

        @Override
        public void handleGET(CoapExchange exchange) {

            // respond to the request
						//System.out.println("req received!");
            exchange.respond("Hello World!");
        }
    }
}
