set key title "Adaptive Counteraction Against Denial of Service Attack"
set nokey
set key left top
set yrange [-3:120] noreverse nowriteback
set xdata time
set linetype 11 lc rgb "green" 
set linetype 12 lc rgb "red"
set timefmt "%Y-%m-%d %H:%M:%S"
data = '< tail -n 20 Counter.log'
plot data u 1:6:($8==0 ? 11 : 12) lc var with lines title "threshold", '' u 1:4 lc rgb "black" with line title "attack"
pause 1
reread
