/*******************************************************************************
 * Copyright (c) 2015 Institute for Pervasive Computing, ETH Zurich and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 * 
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *    http://www.eclipse.org/org/documents/edl-v10.html.
 * 
 * Contributors:
 *    Matthias Kovatsch - creator and main architect
 ******************************************************************************/
package org.eclipse.californium.examples;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.Utils;

//Added following imports //Rikard
import java.net.InetSocketAddress;
import org.eclipse.californium.core.coap.CoAP.Code;
import org.eclipse.californium.core.coap.Request;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.scandium.config.DtlsConnectorConfig;
import org.eclipse.californium.scandium.config.DtlsConnectorConfig.Builder;
import org.eclipse.californium.scandium.DTLSConnector;
import org.eclipse.californium.scandium.dtls.pskstore.StaticPskStore;


public class GETClient {

	private static String identity = "Client_identity"; //Default identity for DTLS //Rikard
	private static String psk = "secretPSK"; //Default PSK for DTLS //Rikard
	private static boolean useProxy = true; //Defaults to NOT going through proxy //Rikard
	private static String uriString = "coaps://127.0.0.1:5556/helloWorld"; //Default Proxy-Uri to use //Rikard

	/*
	 * Application entry point.
	 * 
	 */	
	public static void main(String args[]) {
		//Parse command line arguments //Rikard
		for(int i = 1 ; i < args.length ; i += 2) {
			if(args[i].equals("-id"))
				identity = args[i + 1];
			else if(args[i].equals("-key"))
				psk = args[i + 1];
			else if(args[i].equals("-proxyuri")) {
				useProxy = true;
				uriString = args[i + 1];
			}
		}
		
		URI uri = null; // URI parameter of the request
	
		if (args.length > 0) {
			
			// input URI from command line arguments
			try {
				uri = new URI(args[0]);
			} catch (URISyntaxException e) {
				System.err.println("Invalid URI: " + e.getMessage());
				System.exit(-1);
			}
			
			CoapClient client = new CoapClient(uri);
	
			//Check if this is a coap over DTLS connection //Rikard
			if(uri.toString().contains("coaps")) {
				//Initialize DTLS for client //Rikard
				DTLSConnector dtlsConnector;
				DtlsConnectorConfig.Builder builder = new DtlsConnectorConfig.Builder(new InetSocketAddress(0));
				builder.setPskStore(new StaticPskStore(identity, psk.getBytes()));
				dtlsConnector = new DTLSConnector(builder.build(), null);
				client.setEndpoint(new CoapEndpoint(dtlsConnector, NetworkConfig.getStandard()));
			}

			System.out.print("INFO: Performing CoAP request to " + uri.toString());
			if(useProxy)
				System.out.print(" using it as proxy to reach '" + uriString + "'.");
			System.out.println("");
			if(uri.toString().contains("coaps"))
				System.out.println("INFO: Connection is using DTLS with ID '" + identity + "' and Key '" + psk + "'.");

			//Check if proxy is to be used, if so do an advanced request with the Proxy Uri option set to the target server //Rikard
			CoapResponse response;
			//Decides whether to use proxy and to which target URL
			if(useProxy) {
				Request request = new Request(Code.GET);
				request.getOptions().setProxyUri(uriString);
				response = client.advanced(request);
			}
			else {
				//Else if proxy is not used do normal request //Rikard
				response = client.get();
			}
			
			if (response != null) {
				
				System.out.println(response.getCode());
				System.out.println(response.getOptions());
				System.out.println(response.getResponseText());
				
				System.out.println("\nADVANCED\n");
				// access advanced API with access to more details through .advanced()
				System.out.println(Utils.prettyPrint(response));
				System.exit(0);
			} else {
				System.out.println("No response received.");
			}
			
		} else {
			// display help
			System.out.println("Californium (Cf) GET Client");
			System.out.println("(c) 2014, Institute for Pervasive Computing, ETH Zurich");
			System.out.println();
			System.out.println("Usage: " + GETClient.class.getSimpleName() + " URI");
			System.out.println("  URI: The CoAP URI of the remote resource to GET");
		}
	}

}
