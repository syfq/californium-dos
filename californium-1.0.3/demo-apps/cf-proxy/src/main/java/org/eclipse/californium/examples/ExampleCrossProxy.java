/*******************************************************************************
 * Copyright (c) 2015 Institute for Pervasive Computing, ETH Zurich and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 *
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *    http://www.eclipse.org/org/documents/edl-v10.html.
 *
 * Contributors:
 *    Matthias Kovatsch - creator and main architect
 ******************************************************************************/
package org.eclipse.californium.examples;

import java.io.IOException;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.core.server.resources.CoapExchange;

import org.eclipse.californium.proxy.DirectProxyCoapResolver;
import org.eclipse.californium.proxy.ProxyHttpServer;
import org.eclipse.californium.proxy.resources.ForwardingResource;
import org.eclipse.californium.proxy.resources.ProxyCoapClientResource;
import org.eclipse.californium.proxy.resources.ProxyHttpClientResource;

//Added following imports //Rikard
import java.net.InetSocketAddress;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.scandium.config.DtlsConnectorConfig;
import org.eclipse.californium.scandium.config.DtlsConnectorConfig.Builder;
import org.eclipse.californium.scandium.DTLSConnector;
import org.eclipse.californium.scandium.dtls.pskstore.StaticPskStore;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;

import org.eclipse.californium.core.network.EndpointManager;

//Added following imports //Rikard
import java.lang.Math;

// merged with SMACKClientEnvironment
import java.util.Map;
import java.util.HashMap;
import java.util.Random;
import org.eclipse.californium.SMACKEnvironment.SMACKClientSession;
import org.eclipse.californium.coap.SMACKengine.SMACKEngine;
import java.util.logging.Logger; // syafiq
import org.apache.commons.lang.builder.ToStringBuilder; // debugging purpose //syafiq
import org.apache.commons.lang.builder.ToStringStyle; // debugging purpose //syafiq
import org.eclipse.californium.SMACKEnvironment.SMACKClientEnvironment; //
import org.eclipse.californium.core.coap.CoAP.ResponseCode; // syafiq
import org.eclipse.californium.core.coap.CoAP; // syafiq
import com.google.common.primitives.Ints; //syafiq
import java.nio.ByteBuffer;
import org.eclipse.californium.core.network.stack.SMACKLayer;
import org.eclipse.californium.core.coap.OptionSet;
import org.eclipse.californium.core.coap.Response;
import org.eclipse.californium.core.coap.Request; // syafiq
import java.util.Arrays; // syafiq
import org.eclipse.californium.proxy.resources.ForwardingResource;
import org.eclipse.californium.core.network.stack.SecurityBox;
import org.eclipse.californium.proxy.resources.ForwardingResource;

/**
 * Http2CoAP: Insert in browser:
 *     URI: http://localhost:8080/proxy/coap://localhost:PORT/target
 *
 * CoAP2CoAP: Insert in Copper:
 *     URI: coap://localhost:PORT/coap2coap
 *     Proxy: coap://localhost:PORT/targetA
 *
 * CoAP2Http: Insert in Copper:
 *     URI: coap://localhost:PORT/coap2http
 *     Proxy: http://lantersoft.ch/robots.txt
 */
public class ExampleCrossProxy extends CoapServer {


	//private static final int PORT = NetworkConfig.getStandard().getInt(NetworkConfig.Keys.COAP_PORT);
	private static int COAP_PORT = 5555;
	private static int count = 1;

	private static int securePort= 5684; //Default port DTLS listener runs on //Rikard
  private static String identityClient = "Client_identity"; //Default identity for DTLS (shared with client) //Rikard
  private static String pskClient = "secretPSK"; //Default PSK for DTLS (shared with client) //Rikard

	private static String identity = "Client_identity"; //Default identity for DTLS //Rikard
	private static String psk = "secretPSK"; //Default PSK for DTLS //Rikard

	// future works -> it is possible to have a different identity/key between the proxy & client and between the proxy & server //Rikard, add Syafiq
	private static String identityServer = "Client_identity"; //Default identity for DTLS (shared with server) //Rikard
  private static String pskServer = "secretPSK"; //Default PSK for DTLS (shared with server) //Rikard

	//private static CoapEndpoint SecurityBox.secureEndpointSecbox; //Secure endpoint for DTLS //Rikard
	private static InetSocketAddress localAddressSecure; //Address of DTLS listener //Rikard

	// syafiq
	// private final CoAP.ResponseCode code;
	//private static SMACKClientEnvironment ClientEnvironment;
	private static NetworkConfig config = NetworkConfig.getStandard();
	private static boolean SMACKActive = config.getBoolean(NetworkConfig.Keys.SMACK_ACTIVE);
	private static final int currentSessionID = config.getInt(NetworkConfig.Keys.SMACK_EPOCH_INITIAL_ID);
	private static int SMACKMaxMID = config.getInt(NetworkConfig.Keys.SMACK_MAX_MESSAGE_ID);

	private static String ClientAddr = config.getString(NetworkConfig.Keys.CLIENT_ADDRESS);
	private static String ProxyAddr  = config.getString(NetworkConfig.Keys.PROXY_ADDRESS);
	private static String ServerAddr = config.getString(NetworkConfig.Keys.SERVER_ADDRESS);
  private static String ProxySecurePort = config.getString(NetworkConfig.Keys.PROXY_SECURE_PORT);
  private static String ServerSecurePort = config.getString(NetworkConfig.Keys.SERVER_SECURE_PORT);
  private static String ServerPort = config.getString(NetworkConfig.Keys.SERVER_PORT);

	// SMACK Signalling messages
	private static byte[] SMACKServerToKDC 			 = new byte[] { 0x00, 0x00, };
	private static byte[] SMACKKDCToServer 			 = new byte[] { 0x00, 0x01, };
	private static byte[] SMACKClientToKDC 			 = new byte[] { 0x00, 0x02, };
	private static byte[] SMACKKDCToClient 			 = new byte[] { 0x00, 0x03, };
	private static byte[] SMACKUpdateServertoKDC = new byte[] { 0x00, 0x04, };
	private static byte[] SMACKUpdateKDCToServer = new byte[] { 0x00, 0x05, };

	// State Signalling state
	private static byte[] State_NP_ServerToClient = new byte[] { 0x01, 0x00, };
	private static byte[] State_NP_ServerToKDC 	 	= new byte[] { 0x01, 0x01, };
	private static byte[] State_NP_KDCToServer 	 	= new byte[] { 0x01, 0x02, };
	private static byte[] State_PO_ServerToKDC 	 	= new byte[] { 0x01, 0x03, };
	private static byte[] State_PO_KDCToServer 	 	= new byte[] { 0x01, 0x04, };
	private static byte[] State_PO_KDCToClient		= new byte[] { 0x01, 0x05, };
	private static byte[] State_OP_ServerToKDC 	 	= new byte[] { 0x01, 0x06, };
	private static byte[] State_OP_KDCToServer 	 	= new byte[] { 0x01, 0x07, };
	private static byte[] State_OP_KDCToClient 	 	= new byte[] { 0x01, 0x08, };
	private static byte[] State_PN_ServerToKDC 	 	= new byte[] { 0x01, 0x09, };
	private static byte[] State_PN_KDCToServer 	 	= new byte[] { 0x01, 0x0a, };
	private static byte[] State_PN_KDCToClient 	 	= new byte[] { 0x01, 0x0b, };

	private static byte[] RandomSeed = new byte[32];

	static byte[] KMS;
	protected static int p = config.getInt(NetworkConfig.Keys.SMACK_SESSION_LENGTH); // session length (it must be a prime number)
	private static Integer InputSessionID;
	private static Integer previousSessionID;
	public static Integer InitialMID;
	public static Integer SeedVersionServer;
	public static Integer SeedVersionClient;

	public static void main(String[] args) throws Exception {
		//Parse command line arguments //Rikard

		if (!SecurityBox.secureEndpointSecboxServerSide.isStarted()) {
      try{
        SecurityBox.secureEndpointSecboxServerSide.start();
      } catch (IOException e) {
        System.out.println("start endpoint failed");
      }
    }

		for(int i = 0 ; i < args.length ; i += 2) {
			if(args[i].equals("-p"))
				securePort = Integer.parseInt(args[i + 1]);
			else if(args[i].equals("-id"))
				identityClient = args[i + 1];
			else if(args[i].equals("-key"))
				pskClient = args[i + 1];
			else if(args[i].equals("-Sid"))
				identityServer = args[i + 1];
			else if(args[i].equals("-Skey"))
				pskServer = args[i + 1];
		}

		//for (InetAddress addr : EndpointManager.getEndpointManager().getNetworkInterfaces()) {
		//	// only binds to IPv4 addresses and localhost
		//	if (addr instanceof Inet4Address && addr.isLoopbackAddress()) {
		//		InetSocketAddress bindToAddress = new InetSocketAddress(addr, securePort);
		//		Builder builder = new DtlsConnectorConfig.Builder(bindToAddress);
		//		builder.setPskStore(new StaticPskStore(identity, psk.getBytes()));
		//		SecurityBox.secureEndpointSecbox = new CoapEndpoint(new DTLSConnector(builder.build()), NetworkConfig.getStandard(), false);
		//	}
		//}

		//InetSocketAddress bindToAddress = new InetSocketAddress(ProxyAddr, securePort);
		//Builder builder = new DtlsConnectorConfig.Builder(bindToAddress);
		//builder.setPskStore(new StaticPskStore(identity, psk.getBytes()));
		//SecurityBox.secureEndpointSecbox = new CoapEndpoint(new DTLSConnector(builder.build()), NetworkConfig.getStandard(), false);
		//SecurityBox.secureEndpointSecbox = SecurityBox.SecurityBox.secureEndpointSecboxSecbox;

		// == start KDC server // syafiq
		try {
			ExampleCrossProxy kdc = new ExampleCrossProxy();
			kdc.addEndpoint(SecurityBox.secureEndpointSecbox);
			//kdc.addEndpoint(SecurityBox.secureEndpointSecboxServerSide);
			kdc.start();
			System.out.println("INFO: Started CoAP server with DTLS listener on port " + securePort + "."); //Rikard
			System.out.println("INFO: Accepting incoming DTLS connections using ID '" + identity + "' and Key '" + psk + "'."); //Rikard
		} catch (SocketException e) {
			System.err.println("Failed to initialize server: " + e.getMessage());
		}
		//new ExampleCrossProxy();
	}

	/**
	 * Add individual endpoints listening on default CoAP port on all IPv4 addresses of all network interfaces.
	 */
	//private void addEndpoints() {
	//	for (InetAddress addr : EndpointManager.getEndpointManager().getNetworkInterfaces()) {
	//		// only binds to IPv4 addresses and localhost
	//		if (addr instanceof Inet4Address && addr.isLoopbackAddress()) {
	//			InetSocketAddress bindToAddress = new InetSocketAddress(addr, securePort);
	//			Builder builder = new DtlsConnectorConfig.Builder(bindToAddress);
	//			builder.setPskStore(new StaticPskStore(identity, psk.getBytes()));
	//			SecurityBox.secureEndpointSecbox = new CoapEndpoint(new DTLSConnector(builder.build()), NetworkConfig.getStandard(), false);
	//			addEndpoint(SecurityBox.secureEndpointSecbox);
	//		}
	//	}
	//}

	/*
	 * Constructor for KDC resources. Here, the resources
	 * of the server are initialized.
	 */
	public ExampleCrossProxy() throws SocketException {
		// Proxy act as a relay and cache- phase1
		ForwardingResource coap2coap = new ProxyCoapClientResource("coap2coap", identityServer, pskServer, SecurityBox.secureEndpointSecboxServerSide);
		add(coap2coap);
		// Proxy act as KDC and provide materials - phase0
		add(new SMACKResource());
		add(new StateResource());
	}

	/*
	 * Method to determine if an exchange arrived over DTLS //Rikard
	 */
	private boolean usesDTLS(CoapExchange exchange) {
		return exchange.advanced().getEndpoint().getAddress().getPort() == securePort;
	}

	public void setPrevSessionID (int SessionID) {
		previousSessionID = SessionID;
	}

	public int getPrevSessionID () {
		return previousSessionID;
	}

	public void setSeed() {
		new Random().nextBytes(RandomSeed);
	}

	public byte[] getSeed(){
		return RandomSeed;
	}

	public static void setSeedVersionServer() {
		if (SeedVersionServer == null) {
			SeedVersionServer = 0;
		} else {
			SeedVersionServer++;
		}
	}

	public static void setSeedVersionClient() {
		if (SeedVersionClient == null) {
			SeedVersionClient = 0;
		} else {
			SeedVersionClient++;
		}
	}

	public int getSeedVersionServer() {
		return SeedVersionServer;
	}

	public int getSeedVersionClient() {
		return SeedVersionClient;
	}

	/*
	 * Definition of the SMACK Resources
	 * T = 0, ST = 0 : server -> KDC
	 * T = 0, ST = 1 : KDC -> server
	 * T = 0, ST = 2 : client -> KDC
	 * T = 0, ST = 3 : KDC -> client
	 */
	class SMACKResource extends CoapResource {

		public SMACKResource() {
			// set resource identifier
			super("smack");
			// set display name
			getAttributes().setTitle("SMACK Resource");
		}

		@Override
		public void handleGET(CoapExchange exchange) {
			SecurityBox.setProxyStateNormal();
			byte[] ReceivedPayload = exchange.getRequestPayload();
			//System.out.println("Received ReceivedPayload ="+ToStringBuilder.reflectionToString(ReceivedPayload));
			// Resource for server

			// KM, seed
			byte[] varMasterKey = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, }; // 32 bytes
			//byte[] varSeed = new byte[] { 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, }; // 32 bytes

			if ((Arrays.equals(ReceivedPayload, SMACKServerToKDC)) || (Arrays.equals(ReceivedPayload, SMACKUpdateServertoKDC))) {
				System.out.println("Received request #" + count + " for SMACK resource for server (DTLS: " + usesDTLS(exchange) + ")");
				count++;

				setSeed();
				byte[] varSeed = getSeed();
				System.out.println("varSeed server="+ToStringBuilder.reflectionToString(varSeed));

				byte[] payloadKDCToServer = new byte[SMACKKDCToServer.length + varMasterKey.length + varSeed.length];
				System.arraycopy(SMACKKDCToServer, 0, payloadKDCToServer, 0, SMACKKDCToServer.length);
				System.arraycopy(varMasterKey, 0, payloadKDCToServer, SMACKKDCToServer.length, varMasterKey.length);
				System.arraycopy(varSeed, 0, payloadKDCToServer, (SMACKKDCToServer.length+varMasterKey.length), varSeed.length);

				Response response = new Response(ResponseCode.CONTENT);
				response.setPayload(payloadKDCToServer);
				exchange.respond(response);

			// Resource for client
			} else if (Arrays.equals(ReceivedPayload, SMACKClientToKDC)) {
				System.out.println("Received request #" + count + " for SMACK resource for client (DTLS: " + usesDTLS(exchange) + ")");
				count++;

				if (InputSessionID == null) {
					InputSessionID = currentSessionID;
					InitialMID = InputSessionID;
					setPrevSessionID(InitialMID);
				} else {
					InputSessionID = getPrevSessionID();
					InitialMID = (InputSessionID + p) % (SMACKMaxMID+1);
					setPrevSessionID(InitialMID);
				}

				System.out.println("InitialMID ="+InitialMID);
				byte[] MID = ByteBuffer.allocate(4).putInt(InitialMID).array();

				// KMS
				System.out.println("varSeed client ="+ToStringBuilder.reflectionToString(getSeed()));
				KMS = SMACKEngine.computeKey(varMasterKey, getSeed());
				byte[] SMACKSessionKey = SMACKEngine.computeSessionKeyFromID(KMS, InitialMID);
				System.out.println("SMACKSessionKey ="+ToStringBuilder.reflectionToString(SMACKSessionKey));

				byte[] payloadKDCToClient = new byte[SMACKKDCToClient.length + SMACKSessionKey.length + MID.length];
				System.arraycopy(SMACKKDCToClient, 0, payloadKDCToClient, 0, SMACKKDCToClient.length);
				System.arraycopy(SMACKSessionKey, 0, payloadKDCToClient	, SMACKKDCToClient.length, SMACKSessionKey.length);
				System.arraycopy(MID, 0, payloadKDCToClient, (SMACKKDCToClient.length+SMACKSessionKey.length), MID.length);
				exchange.respond(ResponseCode.CONTENT, payloadKDCToClient);

				if ((SMACKMaxMID-InitialMID) < p) {
					setSeed();
					InputSessionID = null;
				}

			}
		}
	}

	/*
	 * Definition of the State Resources
	 * change state from normal to protected
 	 * ST 0 : server to client
 	 * ST 1 : server to KDC
 	 * ST 2 : ack KDC to Server

 	 * change state from protected to off
 	 * ST 3 : server to KDC
 	 * ST 4 : ack KDC to server

 	 * change state from off to protected
 	 * ST 5 : server to KDC
 	 * ST 6 : ack KDC to server
 	 * ST 7 : KDC to client

 	 * change state from protected to normal
 	 * ST 8 : server to KDC
 	 * ST 9 : ack KDC to server
 	 * ST 10: KDC to all client
	 */
	class StateResource extends CoapResource {

		public StateResource() {
			// set resource identifier
			super("state");
			// set display name
			getAttributes().setTitle("State Resource");
		}

		@Override
		public void handleGET(CoapExchange exchange) {
			byte[] StateServer = Arrays.copyOfRange(exchange.getRequestPayload(), 0, 2);
			//System.out.println("Received StateServer ="+ToStringBuilder.reflectionToString(StateServer));

			// ST 2
			if (Arrays.equals(StateServer, State_NP_ServerToKDC)) {
				SecurityBox.setProxyStateProtected();
				ForwardingResource.setForwarderStateProtected();
				System.out.println("Received request #" + count + " for State Normal to Protected resource for server (DTLS: " + usesDTLS(exchange) + ")");
				count++;

				System.out.println("StateServer"+ToStringBuilder.reflectionToString(StateServer));
				System.out.println("send OK response to server this KDC will act as a relay");

				byte[] payloadKDCToServer = new byte[State_NP_KDCToServer.length];
				System.arraycopy(State_NP_KDCToServer, 0, payloadKDCToServer, 0, State_NP_KDCToServer.length);
				//System.out.println("payloadKDCToServer ="+ToStringBuilder.reflectionToString(payloadKDCToServer));

				Response response = new Response(ResponseCode.CONTENT);
				response.setPayload(payloadKDCToServer);
				exchange.respond(response);

			} else if (Arrays.equals(StateServer, State_PN_ServerToKDC)) {
				SecurityBox.setProxyStateNormal();
				ForwardingResource.setForwarderStateNormal();

				System.out.println("Received request #" + count + " for State Protected to Normal resource for server (DTLS: " + usesDTLS(exchange) + ")");
				count++;

				System.out.println("StateServer"+ToStringBuilder.reflectionToString(StateServer));
				System.out.println("send OK response to server this KDC will BACK TO NORMAL!");

				byte[] payloadKDCToServer = new byte[State_PN_KDCToServer.length];
				System.arraycopy(State_PN_KDCToServer, 0, payloadKDCToServer, 0, State_PN_KDCToServer.length);

				Response response = new Response(ResponseCode.CONTENT);
				response.setPayload(payloadKDCToServer);
				exchange.respond(response);

			} else if (Arrays.equals(StateServer, State_PO_ServerToKDC)) {
				SecurityBox.setProxyStateOff();
				ForwardingResource.setForwarderStateOff();
				System.out.println("Received request #" + count + " for State ST4 resource for server (DTLS: " + usesDTLS(exchange) + ")");
				count++;

				byte[] ArrayServerTimeOff = Arrays.copyOfRange(exchange.getRequestPayload(), 2, 6);
				Integer ServerTimeOff = ByteBuffer.wrap(ArrayServerTimeOff).getInt();
				System.out.println("ServerTimeOff ="+ServerTimeOff);
				SecurityBox.setReceivedTimeOff(ServerTimeOff);

				System.out.println("StateServer"+ToStringBuilder.reflectionToString(StateServer));
				System.out.println("send OK response to server this KDC will CACHE all incoming messages!");

				byte[] payloadKDCToServer = new byte[State_PO_KDCToServer.length];
				System.arraycopy(State_PO_KDCToServer, 0, payloadKDCToServer, 0, State_PO_KDCToServer.length);
				//System.out.println("payloadKDCToServer ="+ToStringBuilder.reflectionToString(payloadKDCToServer));

				Response response = new Response(ResponseCode.CONTENT);
				response.setPayload(payloadKDCToServer);
				exchange.respond(response);
			} else if (Arrays.equals(StateServer, State_OP_ServerToKDC)) {
				System.out.println("Received request #" + count + " for State ST4 resource for server (DTLS: " + usesDTLS(exchange) + ")");
				count++;
				SecurityBox.setProxyStateProtected();
				ForwardingResource.setForwarderStateProtected();

				System.out.println("StateServer"+ToStringBuilder.reflectionToString(StateServer));
				System.out.println("send OK response to server this KDC will go back to PROTECTED from OFF state!");

				byte[] payloadKDCToServer = new byte[State_OP_KDCToServer.length];
				System.arraycopy(State_OP_KDCToServer, 0, payloadKDCToServer, 0, State_OP_KDCToServer.length);

				Response response = new Response(ResponseCode.CONTENT);
				response.setPayload(payloadKDCToServer);
				exchange.respond(response);
				SecurityBox.sendCached();
			}
		}
	}
}
