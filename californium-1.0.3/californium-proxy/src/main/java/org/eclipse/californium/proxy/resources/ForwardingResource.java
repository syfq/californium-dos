/*******************************************************************************
 * Copyright (c) 2015 Institute for Pervasive Computing, ETH Zurich and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 *
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *    http://www.eclipse.org/org/documents/edl-v10.html.
 *
 * Contributors:
 *    Matthias Kovatsch - creator and main architect
 *    Martin Lanter - architect and re-implementation
 *    Francesco Corazza - HTTP cross-proxy
 ******************************************************************************/
package org.eclipse.californium.proxy.resources;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.Request;
import org.eclipse.californium.core.coap.Response;
import org.eclipse.californium.core.network.Exchange;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.network.stack.SecurityBox;
import java.util.Arrays; // syafiq
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.apache.commons.lang.builder.ToStringBuilder; // debugging purpose //Syafiq
import org.apache.commons.lang.builder.ToStringStyle; // debugging purpose //Syafiq

public abstract class ForwardingResource extends CoapResource {

	private static byte[] State_NP_ServerToClient = new byte[] { 0x01, 0x00, };
	private static byte[] State_NP_ServerToKDC 	 	= new byte[] { 0x01, 0x01, };
	private static byte[] State_NP_KDCToServer 	 	= new byte[] { 0x01, 0x02, };
	private static byte[] State_PO_ServerToKDC 	 	= new byte[] { 0x01, 0x03, };
	private static byte[] State_PO_KDCToServer 	 	= new byte[] { 0x01, 0x04, };
	private static byte[] State_PO_KDCToClient		= new byte[] { 0x01, 0x05, };
	private static byte[] State_OP_ServerToKDC 	 	= new byte[] { 0x01, 0x06, };
	private static byte[] State_OP_KDCToServer 	 	= new byte[] { 0x01, 0x07, };
	private static byte[] State_OP_KDCToClient 	 	= new byte[] { 0x01, 0x08, };
	private static byte[] State_PN_ServerToKDC 	 	= new byte[] { 0x01, 0x09, };
	private static byte[] State_PN_KDCToServer 	 	= new byte[] { 0x01, 0x0a, };
	private static byte[] State_PN_KDCToClient 	 	= new byte[] { 0x01, 0x0b, };

	private static byte[] SecureTraffic_Protected = new byte[] { 0x02, 0x00 };
	private static byte[] SecureTraffic_Off				= new byte[] { 0x02, 0x01 };
	private static byte[] TimeInfo_Off						= new byte[] { 0x02, 0x02 };
	private static byte[] ACK_Cached_Request			= new byte[] { 0x02, 0x03 };

	public static boolean isForwarderNormal;
	public static boolean isForwarderProtected;
	public static boolean isForwarderOff;

	public static void setForwarderStateNormal() {
		isForwarderNormal = true;
		isForwarderProtected = false;
		isForwarderOff = false;
	}

	public static void setForwarderStateProtected() {
		isForwarderNormal = false;
		isForwarderProtected = true;
		isForwarderOff = false;
	}

	public static void setForwarderStateOff() {
		isForwarderNormal = false;
		isForwarderProtected = false;
		isForwarderOff = true;
	}

	public ForwardingResource(String resourceIdentifier) {
		super(resourceIdentifier);
	}

	public ForwardingResource(String resourceIdentifier, boolean hidden) {
		super(resourceIdentifier, hidden);
	}

	// a lot of changes here, see per line
	// syafiq
	@Override
	public void handleRequest(Exchange exchange) {
		exchange.sendAccept();
		if (!isForwarderNormal && isForwarderProtected && !isForwarderOff) { // protected
			LOGGER.info("ProxyCoapClient is forwarding a request using DTLS");
			Response response = forwardRequest(exchange, exchange.getRequest());
			//Response response = forwardRequest(exchange);
			exchange.sendResponse(response);
		} else if (isForwarderNormal && !isForwarderProtected && !isForwarderOff) { // normal
			LOGGER.info("This proxy is in normal state, client should access server directly");
			Response normalState = new Response(ResponseCode.CONTENT);
			normalState.setPayload(State_PN_KDCToClient);
			exchange.sendResponse(normalState);
		} else if (!isForwarderNormal && !isForwarderProtected && isForwarderOff) { // off
			LOGGER.info("This proxy is in OFF state!");
			Request incomingRequest = exchange.getRequest();

			// remove the fake uri-path
			// FIXME: HACK // TODO: why? still necessary in new Cf?
			incomingRequest.getOptions().clearUriPath();

			System.out.println("incomingRequest.getPayload => "+ToStringBuilder.reflectionToString(incomingRequest.getPayload()));
			byte[] StateClient = Arrays.copyOfRange(incomingRequest.getPayload(), 0, 2);
			System.out.println("StateClient => "+ToStringBuilder.reflectionToString(StateClient));
			if (Arrays.equals(StateClient, SecureTraffic_Off)) { // client is already in OFF state, it sends cacheable req
				byte[] ArrayClientTimeOff = Arrays.copyOfRange(incomingRequest.getPayload(), 2, 6);
				Integer ClientTimeOff = ByteBuffer.wrap(ArrayClientTimeOff).getInt();
				System.out.println("ClientTimeOff ="+ClientTimeOff);
				Integer ServerTimeOff = SecurityBox.getRemainingOffTime().getSeconds();
				System.out.println("secbox remaining time ="+SecurityBox.getRemainingOffTime().getSeconds());
				if (ClientTimeOff > ServerTimeOff) { // if client accept the OFF time of the server, cache the request
					String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
					System.out.println("["+timeStamp+"] List of MID of request cached ="+exchange.getRequest().getSource());
					while (SecurityBox.listExchange.size() >= 10) {
						SecurityBox.listExchange.remove(0);
					}

					SecurityBox.listExchange.add(exchange);
					System.out.println("["+timeStamp+"] List of MID of cached messages =");
					for (Exchange x:SecurityBox.listExchange) {
						System.out.println("- "+x.getRequest().getMID());
					}
					System.out.println("Exchange Incoming="+SecurityBox.listExchange);

				} else { // client does not accept the server's OFF time, send specific signal
					// send Type 2, Subtype 2 to client
					Response UnsufficientTime = new Response(ResponseCode.CONTENT);
					byte[] OffTimeServer  = ByteBuffer.allocate(4).putInt(ServerTimeOff).array();
					byte[] payloadOffTime = new byte[TimeInfo_Off.length + OffTimeServer.length];
					System.arraycopy(TimeInfo_Off, 0, payloadOffTime, 0, TimeInfo_Off.length);
					System.arraycopy(OffTimeServer, 0, payloadOffTime, TimeInfo_Off.length, OffTimeServer.length);
					UnsufficientTime.setPayload(payloadOffTime);
					exchange.sendResponse(UnsufficientTime);
				}
			} else if (Arrays.equals(StateClient, SecureTraffic_Protected)) { // client is still in protected state
				Response offState = new Response(ResponseCode.CONTENT);
				offState.setPayload(State_PO_KDCToClient);
				exchange.sendResponse(offState);
			} else { // undefined state of client, just for sanity
				Response offState = new Response(ResponseCode.CONTENT);
				offState.setPayload(State_PO_KDCToClient);
				exchange.sendResponse(offState);
			}
		}
	}

	public abstract Response forwardRequest(Exchange exchange, Request request);
	//public abstract Response forwardRequest(Exchange exchange);
}
