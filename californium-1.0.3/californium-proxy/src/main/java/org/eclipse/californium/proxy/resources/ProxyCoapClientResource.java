/*******************************************************************************
 * Copyright (c) 2015 Institute for Pervasive Computing, ETH Zurich and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 *
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *    http://www.eclipse.org/org/documents/edl-v10.html.
 *
 * Contributors:
 *    Matthias Kovatsch - creator and main architect
 *    Martin Lanter - architect and re-implementation
 *    Francesco Corazza - HTTP cross-proxy
 ******************************************************************************/
package org.eclipse.californium.proxy.resources;

import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.coap.Request;
import org.eclipse.californium.core.coap.Response;
import org.eclipse.californium.proxy.CoapTranslator;
import org.eclipse.californium.proxy.TranslationException;
import java.util.concurrent.TimeUnit;
import java.nio.ByteBuffer;

// Additional imports //Syafiq
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import org.eclipse.californium.core.coap.CoAP.Code;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.core.network.EndpointManager;
import org.eclipse.californium.scandium.config.DtlsConnectorConfig;
import org.eclipse.californium.scandium.config.DtlsConnectorConfig.Builder;
import org.eclipse.californium.scandium.DTLSConnector;
import org.eclipse.californium.scandium.dtls.pskstore.StaticPskStore;
import org.apache.commons.lang.builder.ToStringBuilder; // debugging purpose //Syafiq
import org.apache.commons.lang.builder.ToStringStyle; // debugging purpose //Syafiq
import org.eclipse.californium.core.network.Exchange; //Rikard

/**
 * Resource that forwards a coap request with the proxy-uri option set to the
 * desired coap server.
 */
public class ProxyCoapClientResource extends ForwardingResource {

	private String identity;
	private String psk;
	private static CoapEndpoint secureEndpoint;

	public static Request outgoingRequest = null;

	public ProxyCoapClientResource() {
		this("coapClient");
	}

	public ProxyCoapClientResource(String name) {
		// set the resource hidden
		super(name, true);
		getAttributes().setTitle("Forward the requests to a CoAP server.");
	}

	// Overloaded constructor taking identity/key parameters to use for connection to DTLS CoAP server // Rikard
	public ProxyCoapClientResource(String name, String identity, String psk) {
		// set the resource hidden
		super(name, true);
		getAttributes().setTitle("Forward the requests to a CoAP server.");

		// Sets identity/key // Rikard
		this.identity = identity;
		this.psk = psk;
	}

	// Overloaded constructor taking identity/key parameters to use for connection to DTLS CoAP server // Rikard
	public ProxyCoapClientResource(String name, String identity, String psk, CoapEndpoint secureEndpoint) {
		// set the resource hidden
		super(name, true);
		getAttributes().setTitle("Forward the requests to a CoAP server.");

		// Sets identity/key // Rikard
		this.identity = identity;
		this.psk = psk;
		this.secureEndpoint = secureEndpoint;
	}

	@Override
	public Response forwardRequest(Exchange exchange, Request request) {
		//Request incomingRequest = request;
		Request incomingRequest = exchange.getRequest();
		LOGGER.info("ProxyCoAP2CoAP forwards "+incomingRequest);

		LOGGER.info("Request arrived over DTLS: " + (incomingRequest.getSenderIdentity() != null)); //Rikard

		// check the invariant: the request must have the proxy-uri set
		if (!incomingRequest.getOptions().hasProxyUri()) {
			LOGGER.warning("Proxy-uri option not set.");
			return new Response(ResponseCode.BAD_OPTION);
		}

		// remove the fake uri-path
		// FIXME: HACK // TODO: why? still necessary in new Cf?
		incomingRequest.getOptions().clearUriPath();

		// create a new request to forward to the requested coap server
		//public static Request outgoingRequest = null;
		try {

			// create the new request from the original
			outgoingRequest = CoapTranslator.getRequest(incomingRequest);

			// enable response queue for blocking I/O
			// outgoingRequest.enableResponseQueue(true);

			// get the token from the manager // TODO: necessary?
			// outgoingRequest.setToken(TokenManager.getInstance().acquireToken());

			// execute the request
			LOGGER.finer("Sending coap request.");
			// outgoingRequest.execute();
			LOGGER.info("ProxyCoapClient received CoAP request and sends a copy to CoAP target");

			// Create DTLS endpoint that this request will be sent over (if Proxy-Uri specifies to use coaps) //Rikard
			String proxyUriString = URLDecoder.decode(incomingRequest.getOptions().getProxyUri(), "UTF-8");

			//if (isProxy && proxyUriString.contains("coaps") && !isForwarderNormal && isForwarderProtected && !isForwarderOff) {
			outgoingRequest.setToken(incomingRequest.getToken());
			outgoingRequest.setPayload((byte[]) null);
			System.out.println("outgoingRequest ="+outgoingRequest);
			//EndpointManager.getEndpointManager().getDefaultSecureEndpoint().sendRequest(outgoingRequest);
			outgoingRequest.send(secureEndpoint);
			//} else {
				//Else if Proxy-Uri does not specify coaps, send over normal endpoint without DTLS //Rikard
			//	outgoingRequest.send();
			//}

			// accept the request sending a separate response to avoid the
			// timeout in the requesting client
			//LOGGER.finer("Acknowledge message sent");
		} catch (TranslationException e) {
			LOGGER.warning("Proxy-uri option malformed: " + e.getMessage());
			return new Response(CoapTranslator.STATUS_FIELD_MALFORMED);
		} catch (Exception e) {
			LOGGER.warning("Failed to execute request: " + e.getMessage());
			e.printStackTrace(); //For debugging //Rikard
			return new Response(ResponseCode.INTERNAL_SERVER_ERROR);
		}

		try {
			// receive the response // TODO: don't wait for ever
			Response receivedResponse = outgoingRequest.waitForResponse();

			if (receivedResponse != null) {
				LOGGER.finer("Coap response received.");
				// create the real response for the original request
				Response outgoingResponse = CoapTranslator.getResponse(receivedResponse);
				return outgoingResponse;
			} else {
				LOGGER.warning("No response received.");
				return new Response(CoapTranslator.STATUS_TIMEOUT);
			}
		} catch (InterruptedException e) {
			LOGGER.warning("Receiving of response interrupted: " + e.getMessage());
			return new Response(ResponseCode.INTERNAL_SERVER_ERROR);
		}
	}
}
