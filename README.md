![Californium logo](californium-180.png)

Californium-DoS is an enhanced version of [Eclipse Californium](https://github.com/eclipse/californium), a Java implementation of [RFC7252 - Constrained Application Protocol](http://tools.ietf.org/html/rfc7252) for IoT Cloud services. We enhance the vanilla Californium with the capability of Denial of Service attack counteraction. We have developed a cross-layer and
context-aware approach that adaptively counteracts DoS attacks against CoAP server devices, by dynamically adjusting their operative state according to the attack intensity. This considerably limits the impact of DoS attacks and preserves service availability of victim devices to the best possible extent. The proposed approach leverages a trusted Proxy that adaptively shields victim devices, while effectively forwarding and caching messages if needed.

We have made a proof-of-concept implementation of our solution for the Californium [v1.0.3](https://github.com/eclipse/californium/releases/tag/1.0.3) and experimentally evaluated its effectiveness in counteracting DoS and preserving availability of devices under attack.

This is a Master's Thesis Project at [KTH Royal Institute of Technology](https://www.kth.se/en) conducted at [RISE SICS (Swedish Institute of Computer Science)](https://www.sics.se/).

# Compilation

A script to compile is already prepared, simply execute the following from the project's root directory:


```sh
$ sh compile.sh
```

# Documentation

For documentation specific to Californium-DoS, please consult to our [wiki page](https://bitbucket.org/syfq/californium-dos/wiki/Home).

The rest of the documentation related to [Eclipse Californium](https://github.com/eclipse/californium), please consult to their page [here](http://www.eclipse.org/californium/).

# Acknowledgements

[Marco Tiloca](https://www.ri.se/en/marco-tiloca), [Rikard Hoglund](https://www.ri.se/en/rikard-hoglund), and [Viktoria Fodor](https://www.kth.se/profile/vjfodor)

# Contact

A bug, an idea, an issue? contact me directly at atiiq [dot] kth [dot] se or create an issue here on Bitbucket.
